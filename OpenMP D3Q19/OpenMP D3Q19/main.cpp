// OpenMP D3Q19.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include "main.h"
#include <stdio.h>
#define  REFRESH_DELAY 100
int to_disk =10;
int co=0;

int main(int argc, char *argv[])
{
	float Cl=1e-8;
	float nu=(TAU-0.5)/3;//粘度
	float Ct=nu*Cl*Cl/1e-6;
	cout<<"CT "<<Ct<<endl;
	
	initsys();
	InitCuda();
	// 主循环
	//hforce=new float[PARTICLE_NUM*3];
	//hpos=new float[PARTICLE_NUM*2];
	//hu=new float[15*DIST_SIZE];
	/// ////////////////////////////////////////////////////// 
	//timer.starttimer(); //开始计时
	///////////////////////////////////////////////////////////
	//int imax=5000;
	//update(imax);


	//cudaError_t  cudaStatus = cudaMemcpy(hu,dist_out, 15*DIST_SIZE * sizeof(float), cudaMemcpyDeviceToHost);
	//cout<<hu[5*DIST_SIZE+44]<<endl;
	//write_fluid_vtk(100);
	// 释放CUDA资源
	ResetCuda();
	/////////////////////opengl 部分/////////////////
	//glutInit(&argc, argv);
	//glutInitDisplayMode(GLUT_float | GLUT_RGB | GLUT_DEPTH);
	//glutInitWindowPosition(100, 100);
	//glutInitWindowSize(Nx, Ny);
	//glutCreateWindow("OpenGLLuaDemo");
	//glShadeModel(GL_SMOOTH);//平滑着色 
	//glutDisplayFunc(display);
	//glutReshapeFunc(reshape);
	//glutTimerFunc(REFRESH_DELAY, timerEvent,0);
	//glutMainLoop();
	getchar();
	return 0;
}
#include <iostream> // for the use of 'cout'
#include <fstream> // file streams
#include <sstream> // string streams
#include "timer.h"
#include "lbm.h"
#include "particle.h"
#include "params.h"
using namespace std; // permanently use the standard namespace

///////////////////////////////////////////////////////////////////////////////////
// 及时相关
Timer timer;
// 定义GPU内存数组
float *d_rho=0; //密度             DIST_SIZE*GH
float *d_u =0;  //速度u            DIST_SIZE*GH
float *d_v=0;   //速度v            DIST_SIZE*GH
float *d_w=0;   //速度w            DIST_SIZE*GH
float *u_tmp,*v_tmp,*w_tmp;      //DIST_SIZE*GH
float *dist_out=0; //用于写入PDF   DIST_SIZE*GH*9
float *dist_in=0;  //用于读取PDF   DIST_SIZE*GH*9
int* map; //格点标志

// 主机端的
int* hmap; //格点标志
float *hu;
float* hforce;
float* hpos;
float* hq;
float* hpvel;
float* velocity_x, *velocity_y, *velocity_z,*density;
///////////////////////////////////////////////////////////////////////////////////
// 与颗粒有关的量
float *dist_s=0;   // 固体碰撞分布函数
float *pos     ;   //颗粒位置
float *vel     ;   // 颗粒速度
float *psize   ;   // 颗粒半径
float *BS      ;   //整个流场的EPS
float *pBS      ;   //对于于单个颗粒的EPS
float *force   ;  // 颗粒受力
float *Quaternions ; //旋转四元数
float *Quaternions_old ; //旋转四元数
float *particle_mass;//颗粒密度
float *IB  ;        //转动惯量

// opengl
void write_polypoints_vtk(int time, float* qua);
void write_particle_vtk2(int time) ;
void write_fluid_vtk(int time);
void ChecKernelErr();
// 初始化GPU 分配数组空间
void InitCuda()
{
	// Allocate CPU buffers
	d_rho=new float[DIST_SIZE];
	d_u=new float[ DIST_SIZE] ;
	d_v=new float[ DIST_SIZE];
	d_w=new float[DIST_SIZE];
	map=new int[ DIST_SIZE];
	dist_in=new float[ Q*DIST_SIZE ];
	dist_out=new float[ Q*DIST_SIZE ];
	// 颗粒相关
	u_tmp=new float[ DIST_SIZE] ;
	v_tmp=new float[ DIST_SIZE];
	w_tmp=new float[DIST_SIZE];
	BS=new float[DIST_SIZE ];
	pBS=new float[PARTICLE_NUM*PVOLUMESIZE];
	vel=new float[6*PARTICLE_NUM];
	dist_s=new float[ Q*DIST_SIZE];
	pos=new float[3*PARTICLE_NUM ];
	psize=new float[PARTICLE_NUM ];
	force=new float[6*PARTICLE_NUM];
	//颗粒旋转相关的四元数
	//checkCudaErrors(cudaMalloc((void**)&Quaternions, 4*PARTICLE_NUM * sizeof(float)));
	//checkCudaErrors(cudaMalloc((void**)&Quaternions_old, 4*PARTICLE_NUM * sizeof(float)));
	particle_mass=new float[PARTICLE_NUM];
	IB=new float[3*PARTICLE_NUM ];
	// 初始化宏观量 （都设为0）
	// 初始化颗粒
	InitParticle(psize,pos,vel,force,particle_mass,IB);
	// 初始化流场
	PrepareMacroFields(d_rho, d_u, d_v,d_w,map,BS,u_tmp,v_tmp,w_tmp);
	//// 初始化分布函数 设为平衡态
	SetInitialConditions(dist_in,d_u,d_v,d_w,d_rho,map);
	hq=new float[4*PARTICLE_NUM];
	hpvel=new float[6*PARTICLE_NUM];
	timer.starttimer();
	for (int i=0;i<=300;i++)
	{
		//计算流场
		CollideAndPropagate(map,dist_in,dist_out,d_rho,d_u,d_v,d_w,BS,dist_s,0,u_tmp,v_tmp,w_tmp);
		// 边界条件
		ApplyPeriodicBoundaryConditions(dist_out,0);
		ApplyPeriodicBoundaryConditions(dist_out,2);
		CalVelandRho(dist_out,d_rho,d_u,d_v,d_w);
		// 计算颗粒受力
		ParticleEps(psize,pos,BS,pBS);
		ParticleVelTmp(psize,pos,vel,u_tmp,v_tmp,w_tmp,BS,pBS);
		ParticleForce(psize,pos,vel,force,d_u,d_v,d_w,d_rho,dist_out,dist_s,BS,pBS,u_tmp,v_tmp,w_tmp);
		// 更新颗粒位置
		UpdateParticle(psize,pos,vel,force,particle_mass,IB);
		float *tp=dist_in;
		dist_in=dist_out;
		dist_out=tp;
		if (i%50==0)
		{
			cout  << i << "  " <<pos[0] << "  " <<pos[1] <<"  "<<pos[2]<< endl;
			cout  << i << " force  " <<force[0] << "  " <<force[0+PARTICLE_NUM] <<"  "<<force[0+2*PARTICLE_NUM]<<  endl;
			write_particle_vtk2(i); //保存颗粒位置
			write_fluid_vtk(i);
			
		}
		cout<<"#########################################"<<endl;
		cout  << i << "  " <<pos[0] << "  " <<pos[1] <<"  "<<pos[2]<< endl;
		cout  <<"FORCE  "<< i << " force  " <<force[0] << "  " <<force[0+PARTICLE_NUM] <<"  "<<force[0+2*PARTICLE_NUM]<<  endl;
		cout<<"VEL  "<<vel[0]<<"  "<<vel[1]<<endl;
		//getchar();
	}
	timer.stoptimer(1);
	//timer.starttimer();
	//write_fluid_vtk(1);
	for (int j=0;j<1;j++)
	{
		//printf("%i  %f  %f  %f \n",i,hforce[j],hforce[j+PARTICLE_NUM],hforce[j+2*PARTICLE_NUM]);
		cout  << j << " force  " <<force[j] << "  " <<force[j+PARTICLE_NUM] <<"  "<<force[j+2*PARTICLE_NUM]<<  endl;

	}
}

void initsys()
{
	int ignore; // ignore return value of system calls

	ignore = system("rmdir /s /q vtk_fluid"); // create folder if not existing
	ignore = system("rmdir /s /q vtk_particle"); // create folder if not existing
	ignore = system("mkdir  vtk_fluid"); // create folder if not existing
	ignore = system("mkdir  vtk_particle"); // create folder if not existing
	ignore = system("del -f data.dat"); // delete file if existing
}

void ResetCuda()
{
	///////////释放内存 重置设备////////////
	delete map;
	delete d_rho;
	delete d_u;
	delete d_v;
	delete dist_in;
	delete dist_out;
	delete dist_s;
	delete BS;
}

// 将流体数据写入VTK文件
void write_fluid_vtk(int time) {

	/// 从gpu制数据
	velocity_x=d_u;
	velocity_y=d_v;
	velocity_z=d_w;
	density=d_rho;
	/// Create filename
	stringstream output_filename;
	output_filename << "vtk_fluid/fluid_t" << time << ".vtk";
	ofstream output_file;

	/// Open file

	output_file.open(output_filename.str().c_str());

	/// Write VTK header

	output_file << "# vtk DataFile Version 3.0\n";
	output_file << "fluid_state\n";
	output_file << "ASCII\n";
	output_file << "DATASET RECTILINEAR_GRID\n";
	output_file << "DIMENSIONS " << Nx << " " << Ny << " "<<Nz << "\n";

	output_file << "X_COORDINATES " << Nx << " float\n";
	for(int X = 0; X < Nx; ++X) {
		output_file << X + 0.5 << " ";
	}
	output_file << "\n";

	output_file << "Y_COORDINATES " << Ny  << " float\n";
	for(int Y = 1; Y <=Ny ; ++Y) {
		output_file << Y - 0.5 << " ";
	}
	output_file << "\n";

	output_file << "Z_COORDINATES " << Nz << " float\n";
	for(int Z = 1; Z <=Nz ; ++Z) {
		output_file << Z - 0.5 << " ";
	}
	output_file << "\n";

	output_file << "POINT_DATA " << Nx * Ny*Nz << "\n";

	/// Write density difference

	output_file << "SCALARS density_difference float 1\n";
	output_file << "LOOKUP_TABLE default\n";
	for(int Z=1;Z<=Nz;++Z)
		for(int Y = 1; Y <= Ny ; ++Y) {
			for(int X = 1; X <= Nx; ++X) {
				int gid=Z*GX*GY+Y*GX+X;
				output_file << density[gid]  << "\n";
			}
		}

		//// Write velocity

		output_file << "VECTORS velocity_vector float\n";
		for(int Z=1;Z<=Nz;++Z)
			for(int Y = 1; Y <= Ny; ++Y) {
				for(int X = 1; X <= Nx; ++X) {
					int gid=Z*GX*GY+Y*GX+X;
					output_file << velocity_x[gid]  << " " << velocity_y[gid] <<" " << velocity_z[gid]<<" \n";
				}
			}
			//
			/// Close file

			output_file.close();
			// 释放内存
			//delete velocity_x;
			//delete velocity_y;
			//delete density;
			return;
}

///////////////////////////////////////////////////////////////////////
#define POLYPOINTNUM 60
#define CONLINES 90
float polypoints[POLYPOINTNUM][3]={{-0.16246, -2.11803, 1.27598}, {-0.16246, 2.11803, 
	1.27598}, {0.16246, -2.11803, -1.27598}, {0.16246, 
	2.11803, -1.27598}, {-0.262866, -0.809017, -2.32744}, {-0.262866, 
	-2.42705, -0.425325}, {-0.262866, 0.809017, -2.32744}, {-0.262866, 
	2.42705, -0.425325}, {0.262866, -0.809017, 
	2.32744}, {0.262866, -2.42705, 0.425325}, {0.262866, 0.809017, 
	2.32744}, {0.262866, 2.42705, 
	0.425325}, {0.688191, -0.5, -2.32744}, {0.688191, 
	0.5, -2.32744}, {1.21392, -2.11803, 0.425325}, {1.21392, 2.11803, 
	0.425325}, {-2.06457, -0.5, 1.27598}, {-2.06457, 0.5, 
	1.27598}, {-1.37638, -1., 1.80171}, {-1.37638, 1., 
	1.80171}, {-1.37638, -1.61803, -1.27598}, {-1.37638, 
	1.61803, -1.27598}, {-0.688191, -0.5, 2.32744}, {-0.688191, 0.5, 
	2.32744}, {1.37638, -1., -1.80171}, {1.37638, 
	1., -1.80171}, {1.37638, -1.61803, 1.27598}, {1.37638, 1.61803, 
	1.27598}, {-1.7013, 0., -1.80171}, {1.7013, 0., 
	1.80171}, {-1.21392, -2.11803, -0.425325}, {-1.21392, 
	2.11803, -0.425325}, {-1.96417, -0.809017, -1.27598}, {-1.96417, 
	0.809017, -1.27598}, {2.06457, -0.5, -1.27598}, {2.06457, 
	0.5, -1.27598}, {2.22703, -1., -0.425325}, {2.22703, 
	1., -0.425325}, {2.38949, -0.5, 0.425325}, {2.38949, 0.5, 
	0.425325}, {-1.11352, -1.80902, 1.27598}, {-1.11352, 1.80902, 
	1.27598}, {1.11352, -1.80902, -1.27598}, {1.11352, 
	1.80902, -1.27598}, {-2.38949, -0.5, -0.425325}, {-2.38949, 
	0.5, -0.425325}, {-1.63925, -1.80902, 0.425325}, {-1.63925, 1.80902,
	0.425325}, {1.63925, -1.80902, -0.425325}, {1.63925, 
	1.80902, -0.425325}, {1.96417, -0.809017, 1.27598}, {1.96417, 
	0.809017, 1.27598}, {0.850651, 0., 2.32744}, {-2.22703, -1., 
	0.425325}, {-2.22703, 1., 0.425325}, {-0.850651, 
	0., -2.32744}, {-0.525731, -1.61803, -1.80171}, {-0.525731, 
	1.61803, -1.80171}, {0.525731, -1.61803, 1.80171}, {0.525731, 
	1.61803, 1.80171}};

int lines[CONLINES][2]={{1, 10}, {1, 41}, {1, 59}, {2, 12}, {2, 42}, {2, 60}, {3, 6}, {3, 
	43}, {3, 57}, {4, 8}, {4, 44}, {4, 58}, {5, 13}, {5, 56}, {5, 
	57}, {6, 10}, {6, 31}, {7, 14}, {7, 56}, {7, 58}, {8, 12}, {8, 
	32}, {9, 23}, {9, 53}, {9, 59}, {10, 15}, {11, 24}, {11, 53}, {11, 
	60}, {12, 16}, {13, 14}, {13, 25}, {14, 26}, {15, 27}, {15, 
	49}, {16, 28}, {16, 50}, {17, 18}, {17, 19}, {17, 54}, {18, 
	20}, {18, 55}, {19, 23}, {19, 41}, {20, 24}, {20, 42}, {21, 
	31}, {21, 33}, {21, 57}, {22, 32}, {22, 34}, {22, 58}, {23, 
	24}, {25, 35}, {25, 43}, {26, 36}, {26, 44}, {27, 51}, {27, 
	59}, {28, 52}, {28, 60}, {29, 33}, {29, 34}, {29, 56}, {30, 
	51}, {30, 52}, {30, 53}, {31, 47}, {32, 48}, {33, 45}, {34, 
	46}, {35, 36}, {35, 37}, {36, 38}, {37, 39}, {37, 49}, {38, 
	40}, {38, 50}, {39, 40}, {39, 51}, {40, 52}, {41, 47}, {42, 
	48}, {43, 49}, {44, 50}, {45, 46}, {45, 54}, {46, 55}, {47, 
	54}, {48, 55}};


// 将矢量从随体坐标转换到自然坐标
void QM2(float* ow,float *q,float w1,float w2,float w3)
{
	ow[0]=(q[0]*q[0]-q[1]*q[1]-q[2]*q[2]-q[3]*q[3])*w1+2*(q[1]*q[2]-q[0]*q[3])*w2+2*(q[0]*q[2]+q[1]*q[3])*w3;
	ow[1]=2*(q[1]*q[2]+q[0]*q[3])*w1+(q[0]*q[0]-q[1]*q[1]+q[2]*q[2]-q[3]*q[3])*w2+2*(-q[0]*q[1]+q[2]*q[3])*w3;
	ow[2]=2*(-q[0]*q[2]+q[1]*q[3])*w1+2*(q[0]*q[1]+q[2]*q[3])*w2+(q[0]*q[0]-q[1]*q[1]-q[2]*q[2]+q[3]*q[3])*w3;
}
/// ********************************
/// WRITE polypoints STATE TO VTK FILE
/// ********************************

// The polypoints state (node positions) is writen to a VTK file at each t_disk step.
// The following code is designed in such a way that the file can be read by ParaView.

void write_polypoints_vtk(int time, float* qua) {

	//Create filename

	stringstream output_filename;
	output_filename << "vtk_particle/polypoints_t" << time << ".vtk";
	ofstream output_file;
	//	 Open file

	output_file.open(output_filename.str().c_str());

	// Write VTK header

	output_file << "# vtk DataFile Version 3.0\n";
	output_file << "polypoints_state\n";
	output_file << "ASCII\n";
	output_file << "DATASET POLYDATA\n";

	//Write node positions
	float xyz[3];

	output_file << "POINTS " << POLYPOINTNUM << " float\n";

	for(int n = 0; n < POLYPOINTNUM; ++n) {
		QM2(xyz,qua,polypoints[n][0],polypoints[n][1],polypoints[n][2]);
		output_file << xyz[0]+Nx/2.0f << " " << xyz[1]+0.5f+Ny/3.0f <<  " " << xyz[2]+0.5f+Nz/2.0f<<" \n";
	}

	// //Write lines between neighboring nodes

	//output_file << "LINES " << POLYPOINTNUM << " " << 3 * POLYPOINTNUM << "\n";

	//for(int n = 0; n < CONLINES; ++n) {
	//	output_file << "2 " << lines[n][0] << " " << lines[n][1] << "\n";
	//}

	//Write vertices

	output_file << "VERTICES 1 " << POLYPOINTNUM + 1 << "\n";
	output_file << POLYPOINTNUM << " ";

	for(int n = 0; n < POLYPOINTNUM; ++n) {
		output_file << n << " ";
	}

	// Close file

	output_file.close();

	return;
}

void write_particle_vtk2(int time) 
{

	/// 从gpu制数据
	hpos=pos;
	/// Create filename

	stringstream output_filename;
	output_filename << "vtk_particle/particle_t" << time << ".vtk";
	ofstream output_file;

	/// Open file

	output_file.open(output_filename.str().c_str());

	/// Write VTK header

	output_file << "# vtk DataFile Version 3.0\n";
	output_file << "particle_state\n";
	output_file << "ASCII\n";
	output_file << "DATASET POLYDATA\n";

	/// Write node positions

	output_file << "POINTS " << PARTICLE_NUM << " float\n";

	for(int n = 0; n < PARTICLE_NUM; ++n) {
		output_file << hpos[n] << " " << hpos[n+PARTICLE_NUM] << " "<< hpos[n+2*PARTICLE_NUM]<<"\n";
	}

	/// Write lines between neighboring nodes

	/// Write vertices

	output_file << "VERTICES 1 " << PARTICLE_NUM + 1 << "\n";
	output_file << PARTICLE_NUM << " ";

	for(int n = 0; n < PARTICLE_NUM; ++n) {
		output_file << n << " ";
	}

	/// Close file

	output_file.close();
	//delete hpos;
	return;
}
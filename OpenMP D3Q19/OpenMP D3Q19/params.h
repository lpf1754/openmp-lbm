/////////////////////定义模拟参数//////////////////////////
#define Nx 62 //流体域的长
#define Ny 62 //流体域的高
#define Nz 62 //宽
#define GY (Ny+2) // 加一层固壁或ghost
#define GX 64 //(Nx+2+32-(Nx+2)%32) //
#define GZ (Nz+2)
#define THREAD_PER_LINE GX
#define PAGESIZE (GY*GX)
#define DIST_SIZE (GY*GX*GZ)   //DIST_SIZE 是32的整数倍
#define BLOCKSIZE 64    // BLOCK的大小
#define MAXSIZE  8;     // BLOCK的宽度
#define BLOCK_SIZE 64 
#define GRAVITY_X    1.0e-13f //x方向重力
#define GRAVITY_Y    0.0f //x方向重力
#define GRAVITY_Z    0.0f //x方向重力
#define TAU          1.5f  
#define PARTICLE_NUM_X 7
#define PARTICLE_NUM 1 //颗粒数

#define EDGE  8     // 颗粒控制体的边长 （用于计算颗粒受力）,最大取8 （Q取决于使用的GPU)
#define PVOLUMESIZE (EDGE*EDGE*EDGE)    // BLOCK的大小 （用于计算颗粒受力）
#define PPAGESIZE (EDGE*EDGE)    // BLOCK一页的大小 （用于计算颗粒受力）

//D3Q19

#define Q 19
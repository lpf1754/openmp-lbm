#include "param.h"
#include <stdio.h>
#include <cmath>
#include "params.h"
#include <omp.h>
using namespace std;
#define Q 19
//                        0  1    2    3     4    5    6   7   8    9  10  11 12 13 14 15 16 17 18 
//                        C  E    W    N     S    T    B   NE  NW   SE SW  TN TS BN BS TE TW BE BW
const int cx[Q] = {0, 1 , -1 ,  0  ,  0  , 0 ,  0 , 1 , -1,  1, -1, 0,  0, 0, 0, 1,-1, 1,-1}; // lattice weights
const int cy[Q] = {0, 0 ,  0 ,  1  , -1 ,  0 ,  0,  1 ,  1, -1, -1, 1, -1, 1,-1, 0, 0, 0, 0}; // lattice weights
const int cz[Q] = {0, 0 ,  0 ,  0  ,  0  , 1 , -1,  0 ,  0 , 0,  0, 1,  1,-1,-1, 1, 1,-1,-1}; // lattice weights
const int op[Q]=  {0, 2,   1,   4,    3,   6,   5, 10,   9, 8,   7, 14,13,12,11,18,17,16,15};
#ifndef MAX
#define MAX(a,b) (a > b ? a : b)
#endif
#ifndef MIN
#define MIN(a,b) (a > b ? b : a)
#endif
#define SQ(a) pow(a,2)  //定义宏必须非常小心， SQ(a) (a*a) 当 a=b+c 就会变成 SQ(a)= b+c*b+c 完全不是平方的原意 
#define OPTION_SAVE_MACRO_FIELDS 1

template<typename T> bool isfinite(T arg)
{
	return arg == arg && 
		arg != std::numeric_limits<T>::infinity() &&
		arg != -std::numeric_limits<T>::infinity();
}
//// 计算域 100×62×50  对应 128×64×52
//// D3Q15
//#define BLOCK_SIZE 64
//#define DIST_SIZE 425984u   // 等于 128×64×52
//#define OPTION_SAVE_MACRO_FIELDS 1
//#define OPTION_BULK 2
//#define Nx 100
//#define Ny 62
//#define Nz 50
//#define GX 128
//#define GY 64
//#define GZ 52
//#define THREAD_PER_LINE 128
//#define PAGESIZE 8192  //一个xy平面的点数
#define INVALID_NODE 0xffffffff

#define DT 1.0f

typedef struct Dist
{
	float fC;
	float fE;
	float fW;
	float fN;
	float fS;
	float fT;
	float fB;
	float fNE;
	float fNW;
	float fSE;
	float fSW;
	float fTN;
	float fTS;
	float fBN;
	float fBS;
	float fTE;
	float fTW;
	float fBE;
	float fBW;
}
Dist;


// Functions for checking whether a node is of a given specific type.
inline bool is_NTUnused(unsigned int type)
{
	return type == 5;
}

inline bool isNTFullBBWall(unsigned int type)
{
	return type == 1;
}

inline bool is_NTGhost(unsigned int type)
{
	return type == 2;
}

inline bool is_NTPropagationOnly(unsigned int type)
{
	return type == 4;
}

inline bool is_NTFluid(unsigned int type)
{
	return type == 0;
}

// Returns true is the node does not require any special processing
// to calculate macroscopic fields.
inline bool NTUsesStandardMacro(unsigned int type)
{
	return (false || isNTFullBBWall(type) || is_NTFluid(type));
}


// Wet nodes are nodes that undergo a standard collision procedure.
inline bool isWetNode(unsigned int type)
{
	return (false || is_NTFluid(type));
}


// Wet nodes are nodes that undergo a standard collision procedure.
inline bool isExcludedNode(unsigned int type)
{
	return (false || is_NTUnused(type) || is_NTGhost(type));
}

inline bool isPropagationOnly(unsigned int type)
{
	return (false || is_NTPropagationOnly(type));
}

 inline unsigned int decodeNodeType(unsigned int nodetype)
{
	return nodetype;
}

 inline unsigned int decodeNodeOrientation(unsigned int nodetype)
{
	return nodetype;
}

 inline void decodeGlobalIdx(unsigned int gi, int *gx, int *gy, int*gz)
{
	*gz = gi / PAGESIZE;
	int t = gi % PAGESIZE;
	*gy = t / GX;
	*gx = t % GX;
}

inline unsigned int getGlobalIdx(int gx, int gy, int gz)
 {
	 return gx + GX * gy + PAGESIZE * gz;
 }

 /*void die(void)
{
	__asm("trap;");
}*/

 void checkInvalidValues(Dist *d, int gx, int gy, int gz)
{
	bool valid = true;
	if (!isfinite(d->fC))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fC (%f) at: ""(%d, %d, %d)""\n",
			d->fC, gx, gy, gz);
	}

	if (!isfinite(d->fE))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fE (%f) at: ""(%d, %d, %d)""\n",
			d->fE, gx, gy, gz);
	}

	if (!isfinite(d->fW))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fW (%f) at: ""(%d, %d, %d)""\n",
			d->fW, gx, gy, gz);
	}

	if (!isfinite(d->fN))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fN (%f) at: ""(%d, %d, %d)""\n",
			d->fN, gx, gy, gz);
	}

	if (!isfinite(d->fS))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fS (%f) at: ""(%d, %d, %d)""\n",
			d->fS, gx, gy, gz);
	}

	if (!isfinite(d->fT))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fT (%f) at: ""(%d, %d, %d)""\n",
			d->fT, gx, gy, gz);
	}

	if (!isfinite(d->fB))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fB (%f) at: ""(%d, %d, %d)""\n",
			d->fB, gx, gy, gz);
	}

	if (!isfinite(d->fNE))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fNE (%f) at: ""(%d, %d, %d)""\n",
			d->fNE, gx, gy, gz);
	}

	if (!isfinite(d->fNW))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fNW (%f) at: ""(%d, %d, %d)""\n",
			d->fNW, gx, gy, gz);
	}

	if (!isfinite(d->fSE))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fSE (%f) at: ""(%d, %d, %d)""\n",
			d->fSE, gx, gy, gz);
	}

	if (!isfinite(d->fSW))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fSW (%f) at: ""(%d, %d, %d)""\n",
			d->fSW, gx, gy, gz);
	}

	if (!isfinite(d->fTN))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fTN (%f) at: ""(%d, %d, %d)""\n",
			d->fTN, gx, gy, gz);
	}

	if (!isfinite(d->fTS))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fTS (%f) at: ""(%d, %d, %d)""\n",
			d->fTS, gx, gy, gz);
	}

	if (!isfinite(d->fBN))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fBN (%f) at: ""(%d, %d, %d)""\n",
			d->fBN, gx, gy, gz);
	}

	if (!isfinite(d->fBS))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fBS (%f) at: ""(%d, %d, %d)""\n",
			d->fBS, gx, gy, gz);
	}

	if (!isfinite(d->fTE))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fTE (%f) at: ""(%d, %d, %d)""\n",
			d->fTE, gx, gy, gz);
	}

	if (!isfinite(d->fTW))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fTW (%f) at: ""(%d, %d, %d)""\n",
			d->fTW, gx, gy, gz);
	}

	if (!isfinite(d->fBE))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fBE (%f) at: ""(%d, %d, %d)""\n",
			d->fBE, gx, gy, gz);
	}

	if (!isfinite(d->fBW))
	{
		valid = false;
		printf("ERR(subdomain=0): Invalid value of fBW (%f) at: ""(%d, %d, %d)""\n",
			d->fBW, gx, gy, gz);
	}

	if (!valid)
	{
		printf("invalid !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		getchar();
	}
}

// Load the distributions from din to dout, for the node with the index 'idx'.

// Performs propagation when reading distributions from global memory.
// This implements the propagate-on-read scheme.


// Implements the propagate-on-read scheme for the AA access pattern, where the
// distributions are not located in their natural slots, but the opposite ones
// (e.g. fNE is located where fSW normally is). This ensures that within a single
// timestep, the distributions are read from and written to the exact same places
// in global memory.
inline void getDist(Dist *dout, const float *din,unsigned int gi)
{
	dout->fC = din[gi + DIST_SIZE * 0];
	dout->fE = din[gi + DIST_SIZE * 1];
	dout->fW = din[gi + DIST_SIZE * 2];
	dout->fN = din[gi + DIST_SIZE * 3];
	dout->fS = din[gi + DIST_SIZE * 4];
	dout->fT = din[gi + DIST_SIZE * 5];
	dout->fB = din[gi + DIST_SIZE * 6];
	dout->fNE = din[gi + DIST_SIZE * 7];
	dout->fNW = din[gi + DIST_SIZE * 8];
	dout->fSE = din[gi + DIST_SIZE * 9];
	dout->fSW = din[gi + DIST_SIZE * 10];
	dout->fTN = din[gi + DIST_SIZE * 11];
	dout->fTS = din[gi + DIST_SIZE * 12];
	dout->fBN = din[gi + DIST_SIZE * 13];
	dout->fBS = din[gi + DIST_SIZE * 14];
	dout->fTE = din[gi + DIST_SIZE * 15];
	dout->fTW = din[gi + DIST_SIZE * 16];
	dout->fBE = din[gi + DIST_SIZE * 17];
	dout->fBW = din[gi + DIST_SIZE * 18];
}

// Add comments for the Guo density implementation.
inline void bounce_back(Dist *fi)
{
	float t;
	t = fi->fE;
	fi->fE = fi->fW;
	fi->fW = t;
	t = fi->fN;
	fi->fN = fi->fS;
	fi->fS = t;
	t = fi->fT;
	fi->fT = fi->fB;
	fi->fB = t;
	t = fi->fNE;
	fi->fNE = fi->fSW;
	fi->fSW = t;
	t = fi->fNW;
	fi->fNW = fi->fSE;
	fi->fSE = t;
	t = fi->fTN;
	fi->fTN = fi->fBS;
	fi->fBS = t;
	t = fi->fTS;
	fi->fTS = fi->fBN;
	fi->fBN = t;
	t = fi->fTE;
	fi->fTE = fi->fBW;
	fi->fBW = t;
	t = fi->fTW;
	fi->fTW = fi->fBE;
	fi->fBE = t;
}

// Compute the 0th moment of the distributions, i.e. density.
inline void compute_0th_moment(Dist *fi, float *out)
{
	*out = fi->fB + fi->fBE + fi->fBN + fi->fBS + fi->fBW + fi->fC + fi->fE + fi
		->fN + fi->fNE + fi->fNW + fi->fS + fi->fSE + fi->fSW + fi->fT + fi->fTE +
		fi->fTN + fi->fTS + fi->fTW + fi->fW;
}


// Compute the 1st moments of the distributions, i.e. momentum.
 inline void compute_1st_moment(Dist *fi, float *out, int add, float factor)
{
	if (add)
	{
		out[0] += factor *(fi->fBE - fi->fBW + fi->fE + fi->fNE - fi->fNW + fi->fSE
			- fi->fSW + fi->fTE - fi->fTW - fi->fW);
		out[1] += factor *(fi->fBN - fi->fBS + fi->fN + fi->fNE + fi->fNW - fi->fS
			- fi->fSE - fi->fSW + fi->fTN - fi->fTS);
		out[2] += factor *( - fi->fB - fi->fBE - fi->fBN - fi->fBS - fi->fBW + fi
			->fT + fi->fTE + fi->fTN + fi->fTS + fi->fTW);
	}

	else
	{
		out[0] = factor *(fi->fBE - fi->fBW + fi->fE + fi->fNE - fi->fNW + fi->fSE
			- fi->fSW + fi->fTE - fi->fTW - fi->fW);
		out[1] = factor *(fi->fBN - fi->fBS + fi->fN + fi->fNE + fi->fNW - fi->fS -
			fi->fSE - fi->fSW + fi->fTN - fi->fTS);
		out[2] = factor *( - fi->fB - fi->fBE - fi->fBN - fi->fBS - fi->fBW + fi
			->fT + fi->fTE + fi->fTN + fi->fTS + fi->fTW);
	}
}


// Compute the 2nd moments of the distributions.  Order of components is:
// 2D: xx, xy, yy
// 3D: xx, xy, xz, yy, yz, zz
inline void compute_2nd_moment(Dist *fi, float *out)
{
	out[0] = fi->fBE + fi->fBW + fi->fE + fi->fNE + fi->fNW + fi->fSE + fi->fSW +
		fi->fTE + fi->fTW + fi->fW;
	out[1] = fi->fNE - fi->fNW - fi->fSE + fi->fSW;
	out[2] =  - fi->fBE + fi->fBW + fi->fTE - fi->fTW;
	out[3] = fi->fBN + fi->fBS + fi->fN + fi->fNE + fi->fNW + fi->fS + fi->fSE +
		fi->fSW + fi->fTN + fi->fTS;
	out[4] =  - fi->fBN + fi->fBS + fi->fTN - fi->fTS;
	out[5] = fi->fB + fi->fBE + fi->fBN + fi->fBS + fi->fBW + fi->fT + fi->fTE +
		fi->fTN + fi->fTS + fi->fTW;
}


// Computes the 2nd moment of the non-equilibrium distribution function
// given the full distribution fuction 'fi'.
 inline void compute_noneq_2nd_moment(Dist *fi, const float rho,
	float *v0, float *out)
{
	out[0] = fi->fBE + fi->fBW + fi->fE + fi->fNE + fi->fNW + fi->fSE + fi->fSW +
		fi->fTE + fi->fTW + fi->fW - rho *((v0[0] *v0[0]) + 1.0f *(1.0f / 3.0f));
	out[1] = fi->fNE - fi->fNW - fi->fSE + fi->fSW - rho * v0[0] *v0[1];
	out[2] =  - fi->fBE + fi->fBW + fi->fTE - fi->fTW - rho * v0[0] *v0[2];
	out[3] = fi->fBN + fi->fBS + fi->fN + fi->fNE + fi->fNW + fi->fS + fi->fSE +
		fi->fSW + fi->fTN + fi->fTS - rho *((v0[1] *v0[1]) + 1.0f *(1.0f / 3.0f));
	out[4] =  - fi->fBN + fi->fBS + fi->fTN - fi->fTS - rho * v0[1] *v0[2];
	out[5] = fi->fB + fi->fBE + fi->fBN + fi->fBS + fi->fBW + fi->fT + fi->fTE +
		fi->fTN + fi->fTS + fi->fTW - rho *((v0[2] *v0[2]) + 1.0f *(1.0f / 3.0f));
}


// Compute the 1st moments of the distributions and divide it by the 0-th moment
// i.e. compute velocity.
inline void compute_1st_div_0th(Dist *fi, float *out, float zero)
{
	out[0] = (fi->fBE - fi->fBW + fi->fE + fi->fNE - fi->fNW + fi->fSE - fi->fSW
		+ fi->fTE - fi->fTW - fi->fW) / zero;
	out[1] = (fi->fBN - fi->fBS + fi->fN + fi->fNE + fi->fNW - fi->fS - fi->fSE -
		fi->fSW + fi->fTN - fi->fTS) / zero;
	out[2] = ( - fi->fB - fi->fBE - fi->fBN - fi->fBS - fi->fBW + fi->fT + fi
		->fTE + fi->fTN + fi->fTS + fi->fTW) / zero;
}

inline void compute_macro_quant(Dist *fi, float *rho, float *v)
{
	compute_0th_moment(fi, rho);
	compute_1st_div_0th(fi, v,  *rho);
}

 inline void get0thMoment(Dist *fi, int node_type, int orientation,
	float *out)
{
	compute_0th_moment(fi, out);
}


// Common code for the equilibrium and Zou-He density boundary conditions.

//
// Get macroscopic density rho and velocity v given a distribution fi, and
// the node class node_type.
//
inline void getMacro(Dist *fi, int ncode, int node_type, int
	orientation, float *rho, float *v0)
{
	if (NTUsesStandardMacro(node_type))
	{
		compute_macro_quant(fi, rho, v0);
	}
}



// TODO: Check whether it is more efficient to actually recompute
// node_type and orientation instead of passing them as variables.
inline void postcollisionBoundaryConditions(Dist *fi, int ncode, int node_type, int orientation, float *rho, float *v0, unsigned int gi, float*dist_out)
{
	if (0){}
}

 inline void precollisionBoundaryConditions(Dist *fi, int ncode, int node_type, int orientation, float *rho, float *v0)
{
	if (0){}

	else if (isNTFullBBWall(node_type))
	{
		bounce_back(fi);
	}
}

//计算宏观量
 void CalVelandRho(float *  din,float * d_rho,float * du,float* dv,float *dw)
{
	#pragma omp parallel for
	for (int gz=0;gz<GZ;gz++)
		for (int gx=0;gx<GX;gx++)
			for (int gy=0;gy<GY;gy++)
			{
				unsigned int gi = getGlobalIdx(gx, gy, gz);
				// Nothing to do if we're outside of the simulation domain.
				if (gx > Nx+1)
				{
					continue; ;
				}
				// Nothing to do if we're outside of the simulation domain.
				Dist d0;
				float den,v0[3];
				getDist(&d0, din, gi);
				getMacro(&d0,0,0,0,&den,v0);
				d_rho[gi]=den;
				du[gi]=v0[0];
				dv[gi]=v0[1];
				dw[gi]=v0[2];
			}
}
#define BGK
//#define MRT
// Performs the relaxation step in the BGK model given the density rho,
// the velocity v and the distribution fi.
inline void BGK_relaxate0(float rho, float *iv0, Dist *d0, float* fes,float B)
{
	// Body force acceleration.
	float ea0[3];
	ea0[0] = 0.00000000000000000000e+00f;//1.00000000000000008180e-06f;
	ea0[1] = 0.00000000000000000000e+00f;
	ea0[2] = 0.00000000000000000000e+00f;
	float v0[3];
	
	v0[0] = iv0[0] ;//+ 0.5f * ea0[0];
	v0[1] = iv0[1] ;//+ 0.5f * ea0[1];
	v0[2] = iv0[2] ;//+ 0.5f * ea0[2];
	// BGK 单松弛
#ifdef BGK
	Dist feq0;
	//计算平衡分布
	feq0.fC = (1.0f *(1.0f / 3.0f)) *rho *( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0]
	*v0[0]) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) - 3.0f *(1.0f *(1.0f /
		2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f / 3.0f)) *rho;
	feq0.fE = (1.0f *(1.0f / 18.0f)) *rho *(v0[0]*(3.0f *v0[0] + 3.0f) - 3.0f *
		(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2]
	*v0[2])) + (1.0f *(1.0f / 18.0f)) *rho;
	feq0.fW = (1.0f *(1.0f / 18.0f)) *rho *(v0[0]*(3.0f *v0[0] - 3.0f) - 3.0f *
		(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2]
	*v0[2])) + (1.0f *(1.0f / 18.0f)) *rho;
	feq0.fN = (1.0f *(1.0f / 18.0f)) *rho *( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0]
	*v0[0]) + v0[1]*(3.0f *v0[1] + 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2]
	*v0[2])) + (1.0f *(1.0f / 18.0f)) *rho;
	feq0.fS = (1.0f *(1.0f / 18.0f)) *rho *( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0]
	*v0[0]) + v0[1]*(3.0f *v0[1] - 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2]
	*v0[2])) + (1.0f *(1.0f / 18.0f)) *rho;
	feq0.fT = (1.0f *(1.0f / 18.0f)) *rho *( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0]
	*v0[0]) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] +
		3.0f)) + (1.0f *(1.0f / 18.0f)) *rho;
	feq0.fB = (1.0f *(1.0f / 18.0f)) *rho *( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0]
	*v0[0]) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] -
		3.0f)) + (1.0f *(1.0f / 18.0f)) *rho;
	feq0.fNE = (1.0f *(1.0f / 36.0f)) *rho *(v0[0]*(3.0f *v0[0] + 9.0f * v0[1] +
		3.0f) + v0[1]*(3.0f *v0[1] + 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2]
	*v0[2])) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fNW = (1.0f *(1.0f / 36.0f)) *rho *(v0[0]*(3.0f *v0[0] - 9.0f * v0[1] -
		3.0f) + v0[1]*(3.0f *v0[1] + 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2]
	*v0[2])) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fSE = (1.0f *(1.0f / 36.0f)) *rho *(v0[0]*(3.0f *v0[0] - 9.0f * v0[1] +
		3.0f) + v0[1]*(3.0f *v0[1] - 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2]
	*v0[2])) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fSW = (1.0f *(1.0f / 36.0f)) *rho *(v0[0]*(3.0f *v0[0] + 9.0f * v0[1] -
		3.0f) + v0[1]*(3.0f *v0[1] - 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2]
	*v0[2])) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fTN = (1.0f *(1.0f / 36.0f)) *rho *( - 3.0f *(1.0f *(1.0f / 2.0f))*
		(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] + 9.0f * v0[2] + 3.0f) + v0[2]*(3.0f
		*v0[2] + 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fTS = (1.0f *(1.0f / 36.0f)) *rho *( - 3.0f *(1.0f *(1.0f / 2.0f))*
		(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] - 9.0f * v0[2] - 3.0f) + v0[2]*(3.0f
		*v0[2] + 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fBN = (1.0f *(1.0f / 36.0f)) *rho *( - 3.0f *(1.0f *(1.0f / 2.0f))*
		(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] - 9.0f * v0[2] + 3.0f) + v0[2]*(3.0f
		*v0[2] - 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fBS = (1.0f *(1.0f / 36.0f)) *rho *( - 3.0f *(1.0f *(1.0f / 2.0f))*
		(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] + 9.0f * v0[2] - 3.0f) + v0[2]*(3.0f
		*v0[2] - 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fTE = (1.0f *(1.0f / 36.0f)) *rho *(v0[0]*(3.0f *v0[0] + 9.0f * v0[2] +
		3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] +
		3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fTW = (1.0f *(1.0f / 36.0f)) *rho *(v0[0]*(3.0f *v0[0] - 9.0f * v0[2] -
		3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] +
		3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fBE = (1.0f *(1.0f / 36.0f)) *rho *(v0[0]*(3.0f *v0[0] - 9.0f * v0[2] +
		3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] -
		3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
	feq0.fBW = (1.0f *(1.0f / 36.0f)) *rho *(v0[0]*(3.0f *v0[0] + 9.0f * v0[2] -
		3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] -
		3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
	float omega = 1.0f/TAU;
	d0->fC += omega *(feq0.fC - d0->fC)*(1-B)+fes[0];
	d0->fE += omega *(feq0.fE - d0->fE)*(1-B)+fes[1]+(1-B)*(ea0[0]                )/6.0f;
	d0->fW += omega *(feq0.fW - d0->fW)*(1-B)+fes[2]+(1-B)*(-ea0[0]               )/6.0f;
	d0->fN += omega *(feq0.fN - d0->fN)*(1-B)+fes[3]+(1-B)*(         ea0[1]       )/6.0f;
	d0->fS += omega *(feq0.fS - d0->fS)*(1-B)+fes[4]+(1-B)*(        -ea0[1]      )/6.0f;
	d0->fT += omega *(feq0.fT - d0->fT)*(1-B)+fes[5]+(1-B)*(                 ea0[2])/6.0f;
	d0->fB += omega *(feq0.fB - d0->fB)*(1-B)+fes[6]+(1-B)*(                -ea0[2])/6.0f;
	d0->fNE+=omega*(feq0.fNE-d0->fNE)*(1-B)+fes[7]  +(1-B)*(ea0[0]+   ea0[1]        )/12.0f;
	d0->fNW+=omega*(feq0.fNW-d0->fNW)*(1-B)+fes[8]  +(1-B)*(-ea0[0]+  ea0[1]        )/12.0f;
	d0->fSE+=omega*(feq0.fSE-d0->fSE)*(1-B)+fes[9]  +(1-B)*(ea0[0]-  ea0[1]        )/12.0f;
	d0->fSW+=omega*(feq0.fSW-d0->fSW)*(1-B)+fes[10] +(1-B)*(-ea0[0]-  ea0[1]        )/12.0f;
	d0->fTN += omega *(feq0.fTN - d0->fTN)*(1-B)+fes[11]+(1-B)*(     ea0[1]  +ea0[2])/12.0f;
	d0->fTS += omega *(feq0.fTS - d0->fTS)*(1-B)+fes[12]+(1-B)*(     -ea0[1]  +ea0[2])/12.0f;
	d0->fBN += omega *(feq0.fBN - d0->fBN)*(1-B)+fes[13]+(1-B)*(     ea0[1]  -ea0[2])/12.0f;
	d0->fBS += omega *(feq0.fBS - d0->fBS)*(1-B)+fes[14]+(1-B)*(     -ea0[1]  -ea0[2])/12.0f;
	d0->fTE += omega *(feq0.fTE - d0->fTE)*(1-B)+fes[15]+(1-B)*(ea0[0]        +ea0[2])/12.0f;
	d0->fTW += omega *(feq0.fTW - d0->fTW)*(1-B)+fes[16]+(1-B)*(-ea0[0]       +ea0[2])/12.0f;
	d0->fBE += omega *(feq0.fBE - d0->fBE)*(1-B)+fes[17]+(1-B)*(ea0[0]        -ea0[2])/12.0f;
	d0->fBW += omega *(feq0.fBW - d0->fBW)*(1-B)+fes[18]+(1-B)*(-ea0[0]        -ea0[2])/12.0f;
#endif
	// MRT 多松弛
#ifdef MRT
	float *f=(float*)d0; //结构体的指针转换为数组指针
	float m[Q]; // 存储各阶矩
	// 计算各阶矩
	m[0]=rho;
	m[1]=f[1] - f[2] + f[7] - f[8] + f[9] - f[10] + f[15] - f[16] + f[17] - f[18];
	m[2]=f[3] - f[4] + f[7] + f[8] - f[9] - f[10] + f[11] - f[12] + f[13] - f[14];
	m[3]=f[5] - f[6] + f[11] + f[12] - f[13] - f[14] + f[15] + f[16] - f[17] - f[18];
	m[4]=-f[0] + f[7] + f[8] + f[9] + f[10] + f[11] + f[12] + f[13] + f[14] + f[15] + f[16] + f[17] + f[18];
	m[5]=2*f[1] + 2*f[2] - f[3] - f[4] - f[5] - f[6] + f[7] + f[8] + f[9] + f[10] - 2*f[11] - 2*f[12] - 2*f[13] - 2*f[14] + f[15] + f[16] + f[17] + f[18];
	m[6]=f[3] + f[4] - f[5] - f[6] + f[7] + f[8] + f[9] + f[10] - f[15] - f[16] - f[17] - f[18];
	m[7]=f[7] - f[8] - f[9] + f[10];
	m[8]=f[11] - f[12] - f[13] + f[14];
	m[9]=f[15] - f[16] - f[17] + f[18];
	m[10]=-2*f[1] + 2*f[2] + f[7] - f[8] + f[9] - f[10] + f[15] - f[16] + f[17] - f[18];
	m[11]=-2*f[3] + 2*f[4] + f[7] + f[8] - f[9] - f[10] + f[11] - f[12] + f[13] - f[14];
	m[12]=-2*f[5] + 2*f[6] + f[11] + f[12] - f[13] - f[14] + f[15] + f[16] - f[17] - f[18];
	m[13]=f[7] - f[8] + f[9] - f[10] - f[15] + f[16] - f[17] + f[18];
	m[14]=-f[7] - f[8] + f[9] + f[10] + f[11] - f[12] + f[13] - f[14];
	m[15]=-f[11] - f[12] + f[13] + f[14] + f[15] + f[16] - f[17] - f[18];
	m[16]=f[0] - 2*f[1] - 2*f[2] - 2*f[3] - 2*f[4] - 2*f[5] - 2*f[6] + f[7] + f[8] + f[9] + f[10] + f[11] + f[12] + f[13] + f[14] + f[15] + f[16] +f[17] + f[18];
	m[17]=-2*f[1] - 2*f[2] + f[3] + f[4] + f[5] + f[6] + f[7] + f[8] + f[9] + f[10] - 2*f[11] - 2*f[12] - 2*f[13] - 2*f[14] + f[15] + f[16] + f[17] + f[18];
	m[18]=-f[3] - f[4] + f[5] + f[6] + f[7] + f[8] + f[9] + f[10] - f[15] - f[16] - f[17] - f[18];
	// 计算各阶平衡矩
	float meq4=(v0[0]*v0[0] + v0[1]*v0[1] + v0[2]*v0[2])*rho;
	float meq5=(2*v0[0]*v0[0]-v0[1]*v0[1]-v0[2]*v0[2])*rho;
	float meq6=(v0[1]*v0[1]-v0[2]*v0[2])*rho;
	float meq7=v0[0]*v0[1]*rho;
	float meq8=v0[1]*v0[2]*rho;
	float meq9=v0[0]*v0[2]*rho;
	// 在矩空间中碰撞
	float KIA=3.0/16.0;
	float w0=1/TAU;
	float w1=(4-2*w0)/(2-w0+4*KIA*w0);
	float gama_even=1.0-w0*(1-B);
	float gama_odd=1-w1*(1-B);
	m[4]=meq4+gama_even*(m[4]-meq4);
	m[5]=meq5+gama_even*(m[5]-meq5);
	m[6]=meq6+gama_even*(m[6]-meq6);
	m[7]=meq7+gama_even*(m[7]-meq7);
	m[8]=meq8+gama_even*(m[8]-meq8);
	m[9]=meq9+gama_even*(m[9]-meq9);
	m[10] = gama_odd  * m[10];
	m[11] = gama_odd  * m[11];
	m[12] = gama_odd  * m[12];
	m[13] = gama_odd  * m[13];
	m[14] = gama_odd  * m[14];
	m[15] = gama_odd  * m[15];
	m[16] = gama_even * m[16];
	m[17] = gama_even * m[17];
	m[18] = gama_even * m[18];
	// 变换回分布函数
	f[0]=m[0]/3 - m[4]/2 + m[16]/6+fes[0];
	f[1]=m[0]/18 + m[1]/6 + m[5]/12 - m[10]/6 - m[16]/18 - m[17]/12+(1-B)*(ea0[0]                )/6.0f+fes[1];
	f[2]=m[0]/18 - m[1]/6 + m[5]/12 + m[10]/6 - m[16]/18 - m[17]/12+(1-B)*(-ea0[0]                )/6.0f+fes[2];
	f[3]=m[0]/18 + m[2]/6 - m[5]/24 + m[6]/8 - m[11]/6 - m[16]/18 + m[17]/24 - m[18]/8+(1-B)*(         ea0[1]       )/6.0f+fes[3];
	f[4]=m[0]/18 - m[2]/6 - m[5]/24 + m[6]/8 + m[11]/6 - m[16]/18 + m[17]/24 - m[18]/8+(1-B)*(        -ea0[1]      )/6.0f+fes[4];
	f[5]=m[0]/18 + m[3]/6 - m[5]/24 - m[6]/8 - m[12]/6 - m[16]/18 + m[17]/24 + m[18]/8+(1-B)*(                 ea0[2])/6.0f+fes[5];
	f[6]=m[0]/18 - m[3]/6 - m[5]/24 - m[6]/8 + m[12]/6 - m[16]/18 + m[17]/24 + m[18]/8+(1-B)*(                 -ea0[2])/6.0f+fes[6];
	f[7]=m[0]/36 + m[1]/12 + m[2]/12 + m[4]/24 + m[5]/48 + m[6]/16 + m[7]/4 + m[10]/24 + m[11]/24 + m[13]/8 - m[14]/8 + m[16]/72 + m[17]/48 + m[18]/16 +(1-B)*(ea0[0]+   ea0[1]        )/12.0f+fes[7];
	f[8]=m[0]/36 - m[1]/12 + m[2]/12 + m[4]/24 + m[5]/48 + m[6]/16 - m[7]/4 - m[10]/24 + m[11]/24 - m[13]/8 - m[14]/8 + m[16]/72 + m[17]/48 + m[18]/16 +(1-B)*(-ea0[0]+   ea0[1]        )/12.0f+fes[8];
	f[9]=m[0]/36 + m[1]/12 - m[2]/12 + m[4]/24 + m[5]/48 + m[6]/16 - m[7]/4 + m[10]/24 - m[11]/24 + m[13]/8 + m[14]/8 + m[16]/72 + m[17]/48 + m[18]/16 +(1-B)*(ea0[0]-  ea0[1]        )/12.0f+fes[9];
	f[10]=m[0]/36 - m[1]/12 - m[2]/12 + m[4]/24 + m[5]/48 + m[6]/16 + m[7]/4 - m[10]/24 - m[11]/24 - m[13]/8 + m[14]/8 + m[16]/72 + m[17]/48 + m[18]/16 +(1-B)*(-ea0[0]-  ea0[1]        )/12.0f+fes[10];
	f[11]=m[0]/36 + m[2]/12 + m[3]/12 + m[4]/24 - m[5]/24 + m[8]/4 + m[11]/24 + m[12]/24 + m[14]/8 - m[15]/8 + m[16]/72 - m[17]/24+(1-B)*(     ea0[1]  +ea0[2])/12.0f+fes[11];
	f[12]=m[0]/36 - m[2]/12 + m[3]/12 + m[4]/24 - m[5]/24 - m[8]/4 - m[11]/24 + m[12]/24 - m[14]/8 - m[15]/8 + m[16]/72 - m[17]/24+(1-B)*(     -ea0[1]  +ea0[2])/12.0f+fes[12];
	f[13]=m[0]/36 + m[2]/12 - m[3]/12 + m[4]/24 - m[5]/24 - m[8]/4 + m[11]/24 - m[12]/24 + m[14]/8 + m[15]/8 + m[16]/72 - m[17]/24+(1-B)*(     ea0[1]  -ea0[2])/12.0f+fes[13];
	f[14]=m[0]/36 - m[2]/12 - m[3]/12 + m[4]/24 - m[5]/24 + m[8]/4 - m[11]/24 - m[12]/24 - m[14]/8 + m[15]/8 + m[16]/72 - m[17]/24+(1-B)*(     -ea0[1]  -ea0[2])/12.0f+fes[14];
	f[15]=m[0]/36 + m[1]/12 + m[3]/12 + m[4]/24 + m[5]/48 - m[6]/16 + m[9]/4 + m[10]/24 + m[12]/24 - m[13]/8 + m[15]/8 + m[16]/72 + m[17]/48 - m[18]/16+(1-B)*(ea0[0]       +ea0[2])/12.0f+fes[15];
	f[16]=m[0]/36 - m[1]/12 + m[3]/12 + m[4]/24 + m[5]/48 - m[6]/16 - m[9]/4 - m[10]/24 + m[12]/24 + m[13]/8 + m[15]/8 + m[16]/72 + m[17]/48 - m[18]/16+(1-B)*(-ea0[0]       +ea0[2])/12.0f+fes[16];
	f[17]=m[0]/36 + m[1]/12 - m[3]/12 + m[4]/24 + m[5]/48 - m[6]/16 - m[9]/4 + m[10]/24 - m[12]/24 - m[13]/8 - m[15]/8 + m[16]/72 + m[17]/48 - m[18]/16+(1-B)*(ea0[0]        -ea0[2])/12.0f+fes[17];
	f[18]=m[0]/36 - m[1]/12 - m[3]/12 + m[4]/24 + m[5]/48 - m[6]/16 + m[9]/4 - m[10]/24 - m[12]/24 + m[13]/8 - m[15]/8 + m[16]/72 + m[17]/48 - m[18]/16+(1-B)*(-ea0[0]        -ea0[2])/12.0f+fes[18];
#endif
	// FIXME: This should be moved to postcollision boundary conditions.
	/*iv0[0] += 0.5f * ea0[0];
	iv0[1] += 0.5f * ea0[1];
	iv0[2] += 0.5f * ea0[2];*/
}

// A kernel to set the node distributions using the equilibrium distributions
// and the macroscopic fields.
void SetInitialConditions(float *dist1_in, float * ivx,
	float *ivy, float * ivz, const float *
	irho, const int * map)
{
	// This is a workaround for the limitations of current CUDA devices.
	// We would like the grid to be 3 dimensional, but only 2 dimensions
	// are supported.  We thus encode the first two dimensions (x, y) of
	// the simulation grid into the x dimension of the CUDA/OpenCL grid
	// as:
	//   x_dev = y * num_blocks + x.
	//
	// This works fine, as x is relatively small, since:
	//   x = x_sim / block_size.
	#pragma omp parallel for
	for (int gz=0;gz<GZ;gz++)
		for (int gx=0;gx<GX;gx++)
			for (int gy=0;gy<GY;gy++)
			{
				unsigned int gi = getGlobalIdx(gx, gy, gz);
				// Nothing to do if we're outside of the simulation domain.
				if (gx > Nx+1)
				{
					continue; ;
				}
				// Cache macroscopic fields in local variables.
				float rho = irho[gi];
				float v0[3];
				v0[0] = ivx[gi];
				v0[1] = ivy[gi];
				v0[2] = ivz[gi];
				dist1_in[gi + (0*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 3.0f)) *rho *( -
					3.0f *(1.0f *(1.0f / 2.0f))*(v0[0] *v0[0]) - 3.0f *(1.0f *(1.0f / 2.0f))*
					(v0[1] *v0[1]) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f
					/ 3.0f)) *rho;
				dist1_in[gi + (1*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 18.0f)) *rho
					*(v0[0]*(3.0f *v0[0] + 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) -
					3.0f *(1.0f *(1.0f / 2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f / 18.0f)) *rho;
				dist1_in[gi + (2*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 18.0f)) *rho
					*(v0[0]*(3.0f *v0[0] - 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[1] *v0[1]) -
					3.0f *(1.0f *(1.0f / 2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f / 18.0f)) *rho;
				dist1_in[gi + (3*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 18.0f)) *rho
					*( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] + 3.0f)
					- 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f / 18.0f)) *rho;
				dist1_in[gi + (4*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 18.0f)) *rho
					*( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] - 3.0f)
					- 3.0f *(1.0f *(1.0f / 2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f / 18.0f)) *rho;
				dist1_in[gi + (5*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 18.0f)) *rho
					*( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0] *v0[0]) - 3.0f *(1.0f *(1.0f / 2.0f)
					)*(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] + 3.0f)) + (1.0f *(1.0f / 18.0f))
					*rho;
				dist1_in[gi + (6*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 18.0f)) *rho
					*( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0] *v0[0]) - 3.0f *(1.0f *(1.0f / 2.0f)
					)*(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] - 3.0f)) + (1.0f *(1.0f / 18.0f))
					*rho;
				dist1_in[gi + (7*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*(v0[0]*(3.0f *v0[0] + 9.0f * v0[1] + 3.0f) + v0[1]*(3.0f *v0[1] + 3.0f) -
					3.0f *(1.0f *(1.0f / 2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (8*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*(v0[0]*(3.0f *v0[0] - 9.0f * v0[1] - 3.0f) + v0[1]*(3.0f *v0[1] + 3.0f) -
					3.0f *(1.0f *(1.0f / 2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (9*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*(v0[0]*(3.0f *v0[0] - 9.0f * v0[1] + 3.0f) + v0[1]*(3.0f *v0[1] - 3.0f) -
					3.0f *(1.0f *(1.0f / 2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (10*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*(v0[0]*(3.0f *v0[0] + 9.0f * v0[1] - 3.0f) + v0[1]*(3.0f *v0[1] - 3.0f) -
					3.0f *(1.0f *(1.0f / 2.0f))*(v0[2] *v0[2])) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (11*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] + 9.0f
					* v0[2] + 3.0f) + v0[2]*(3.0f *v0[2] + 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (12*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] - 9.0f
					* v0[2] - 3.0f) + v0[2]*(3.0f *v0[2] + 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (13*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] - 9.0f
					* v0[2] + 3.0f) + v0[2]*(3.0f *v0[2] - 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (14*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*( - 3.0f *(1.0f *(1.0f / 2.0f))*(v0[0] *v0[0]) + v0[1]*(3.0f *v0[1] + 9.0f
					* v0[2] - 3.0f) + v0[2]*(3.0f *v0[2] - 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (15*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*(v0[0]*(3.0f *v0[0] + 9.0f * v0[2] + 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*
					(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] + 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (16*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*(v0[0]*(3.0f *v0[0] - 9.0f * v0[2] - 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*
					(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] + 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (17*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*(v0[0]*(3.0f *v0[0] - 9.0f * v0[2] + 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*
					(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] - 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
				dist1_in[gi + (18*DIST_SIZE + (unsigned int)(0+0))] = (1.0f *(1.0f / 36.0f)) *rho
					*(v0[0]*(3.0f *v0[0] + 9.0f * v0[2] - 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*
					(v0[1] *v0[1]) + v0[2]*(3.0f *v0[2] - 3.0f)) + (1.0f *(1.0f / 36.0f)) *rho;
			}
}

void PrepareMacroFields(float *orho, float *u,float *v ,float* w,int *map,float* BS,float*u_tmp,float*v_tmp,float*w_tmp)
{
	#pragma omp parallel for
	for (int gz=0;gz<GZ;gz++)
		for (int gx=0;gx<GX;gx++)
			for (int gy=0;gy<GY;gy++)
			{
				int gi = getGlobalIdx(gx, gy, gz);
				// Nothing to do if we're outside of the simulation domain.
				if (gx > GX-1)
				{
					continue; ;
				}
				
				map[gi]=0;
				u_tmp[gi]=0;
				v_tmp[gi]=0;
				w_tmp[gi]=0;
				u[gi]=0.0f;
				v[gi]=0.0f;
				w[gi]=0.0f;
				orho[gi]=1.0f;
				
				BS[gi]=0.0f;
	
	
				// 四个固壁面
				if (gy==0 || gy==Ny+1)
				{
					map[gi]=1;
				}

				if (gz==0 || gz==Nz+1)
				{
					map[gi]=2; //2代表ghost点
				}

				// 两个周期面
				if (gx==0 || gx==Nx+1)
				{
					map[gi]=2; //2代表ghost点
				}
				//中间一个r=5球体
				//float r0[]={Nx/2.0f,Ny/2.0f,Nz/2.0f};
				//if (SQ(gx-r0[0])+SQ(gy-r0[1])+SQ(gz-r0[2])<16.0f)
				//{
				//	//printf("%d %d %d\n",gx,gy,gz);
				//	map[gi]=1;
				//}
				if (gx>Nx+1) map[gi]=5;           //超出模拟域的
			}
}

void CollideAndPropagate(const int * map,float *dist_in,float *dist_out,
	float * gg0m0,float * ovx,float * ovy,float * ovz, float *BS,float *dist_s, int options,
	float*u_tmp,float*v_tmp,float*w_tmp )
{
	// This is a workaround for the limitations of current CUDA devices.
	// We would like the grid to be 3 dimensional, but only 2 dimensions
	// are supported.  We thus encode the first two dimensions (x, y) of
	// the simulation grid into the x dimension of the CUDA/OpenCL grid
	// as:
	//   x_dev = y * num_blocks + x.
	//
	// This works fine, as x is relatively small, since:
	//   x = x_sim / block_size.
	//printf("#############################\n");
	
#ifndef _OPENMP
	fprintf(stderr, "OpenMP not supported");
#endif
#pragma omp parallel for
	for (int gz=0;gz<GZ;gz++)
		for (int gx=0;gx<GX;gx++)
			for (int gy=0;gy<GY;gy++)
			{
				//printf("%d %d %d \n",gx,gy,gz);
				unsigned int gi = getGlobalIdx(gx, gy, gz);
				//// Nothing to do if we're outside of the simulation domain.
				if (gx > Nx+1)
				{
					continue; ;
				}
				//
				int ncode = map[gi];
				int type = decodeNodeType(ncode);
				// Unused nodes do not participate in the simulation.
				if (isExcludedNode(type))
				{
					continue; ;
				}
				//printf("%d %d %d %d \n",gx,gy,gz,ncode);
				int orientation = decodeNodeOrientation(ncode);
				//// Cache the distributions in local variables
				Dist d0;
				//
				if (!isPropagationOnly(type))
				{
				//	//printf("%d %d %d \n",gx,gy,gz);
					getDist(&d0, dist_in, gi);
				//	printf("%d %d %d %d %f\n",gx,gy,gz,ncode,d0.fC);
				//	getchar();
				//	// Macroscopic quantities for the current cell
					float g0m0, v[3];
					precollisionBoundaryConditions(&d0, ncode, type, orientation, &g0m0, v);
					if (isWetNode(type))
					{
						g0m0=gg0m0[gi];
						v[0]=ovx[gi];
						v[1]=ovy[gi];
						v[2]=ovz[gi];
						float B=BS[gi];  // 碰撞项的权系数
						float fes[]={0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};   //存储固体碰撞项
						if (B>0.0f) // 读入碰撞项
						{
							//printf("%d %d %d %f \n",gx,gy,gi,B);
							for (int k=0;k<Q;k++)
							{
								fes[k]=dist_s[gi+k*DIST_SIZE];
								//printf("pdf %d %d %d %f \n",gx,gy,k,ws[k]);
							}
							// 重置 BS
							BS[gi]=0;
							// 重置速度tmp
							u_tmp[gi]=0;
							v_tmp[gi]=0;
							w_tmp[gi]=0;
						}
						BGK_relaxate0(g0m0, v, &d0,fes,B);
					}

					if (isWetNode(type))
					{
						checkInvalidValues(&d0, gx, gy, gz);
					}


				//	// Only save the macroscopic quantities if requested to do so.
					if ((options &OPTION_SAVE_MACRO_FIELDS) && isWetNode(type))
					{
						gg0m0[gi] = g0m0;
						ovx[gi] = v[0];
						ovy[gi] = v[1];
						ovz[gi] = v[2];
					}

				//	//迁移   经测试 速度较慢
					float *f=(float*)&d0;
					//#pragma omp parallel for
					for (int k=0;k<Q;k++)
					{
				//		//if (k==0 || k==3 || k==4 || k==5 || k==6 || k==11 || k==12 || k==13 || k==14) continue;  //测试东边 || k=11 || k==12 || k==13 || k==14
				//		//if (k==1 || k==7 || k==9 || k==15 || k==17 ) continue;  //测试西边 || k==7 || k==9 || k==15 || k==17 
						int di=gx+cx[k];
						int dj=gy+cy[k];
						int dk=gz+cz[k];
						if (dj>=0 && dj<=GY && di>=0 && di<=(Nx+1) && dk>=0 && dk<=GZ)
						{
							dist_out[gi+k*DIST_SIZE+cz[k]*PAGESIZE+cy[k]*GX+cx[k]]=f[k];
						}

					}
				}

			}
}
// Applies periodic boundary conditions within a single subdomain.
//  dist: pointer to the distributions array
//  axis: along which axis the PBCs are to be applied (0:x, 1:y, 2:z)
void ApplyPeriodicBoundaryConditions(float *dist, int axis)
{
	
	// For single block PBC, the envelope size (width of the ghost node
	// layer) is always 1.
	// TODO(michalj): Generalize this for the case when envelope_size != 1.
	if (axis == 0)
	{
		#pragma omp parallel for
		for (int idx1=0;idx1<GZ;idx1++)
			for (int idx2=0;idx2<GY;idx2++)
			{
				if (idx1 >= Ny+2 || idx2 >= Nz+2)
				{
					continue ;
				}
				unsigned int gi_low, gi_high;
				gi_low = getGlobalIdx(0, idx1, idx2); // ghost node
				gi_high = getGlobalIdx(Nx, idx1, idx2); // real node

				{
					// TODO(michalj): Generalize this for grids with e_i > 1.
					// Load distributions to be propagated from low idx to high idx.

					const float ffW = dist[gi_low  + DIST_SIZE * 2];
					const float ffNW = dist[gi_low + DIST_SIZE * 8];
					const float ffSW = dist[gi_low + DIST_SIZE * 10];
					const float ffTW = dist[gi_low + DIST_SIZE * 16];
					const float ffBW = dist[gi_low + DIST_SIZE * 18];
					if (gi_high != INVALID_NODE && isfinite(ffW))
					{
						dist[gi_high + DIST_SIZE * 2] = ffW;
					}

					if (isfinite(ffNW))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 > 1)
						{
							if (idx1==Ny+1)
							{
								int gi_high2=getGlobalIdx(Nx, 1, idx2); // ghost node
								dist[gi_high2 + DIST_SIZE * 8] = ffNW;
							}
							else
								dist[gi_high + DIST_SIZE * 8] = ffNW;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffSW))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 < Ny)
						{
							if (idx1==0)
							{
								int gi_high2=getGlobalIdx(Nx, Ny, idx2); // ghost node
								dist[gi_high2 + DIST_SIZE * 10] = ffSW;
							}
							else
								dist[gi_high + DIST_SIZE * 10] = ffSW;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffTW))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 > 1)
						{
							if (idx2==Nz+1)
							{
								int gi_high2=getGlobalIdx(Nx, idx1, 1); // ghost node
								dist[gi_high2 + DIST_SIZE * 16] = ffTW;
							}
							else
								dist[gi_high + DIST_SIZE * 16] = ffTW;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffBW))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 < Nz)
						{
							if (idx2==0)
							{
								int gi_high2=getGlobalIdx(Nx, idx1, Nz); // ghost node
								dist[gi_high2 + DIST_SIZE * 18] = ffBW;
							}
							else
								dist[gi_high + DIST_SIZE * 18] = ffBW;
						}

						else
						{
							if (0){}
						}
					}


				}

				// low to high
				{
					// Load distributrions to be propagated from high idx to low idx.
					const float ffE = dist[gi_high + DIST_SIZE * 1+(unsigned int)1];
					const float ffNE = dist[gi_high + DIST_SIZE * 7+(unsigned int)1];
					const float ffSE = dist[gi_high + DIST_SIZE * 9+(unsigned int)1];
					const float ffTE = dist[gi_high + DIST_SIZE * 15+(unsigned int)1];
					const float ffBE = dist[gi_high + DIST_SIZE * 17+(unsigned int)1];
					if (isfinite(ffE) && gi_low != INVALID_NODE)
					{
						dist[gi_low + DIST_SIZE * 1+(unsigned int)1] = ffE;
					}

					if (isfinite(ffNE))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 > 1)
						{
							if (idx1==Ny+1)
							{
								int gi_high2=getGlobalIdx(1, 1, idx2); // ghost node
								dist[gi_high2 + DIST_SIZE * 7] = ffNE;
							}
							else
								dist[gi_low + DIST_SIZE * 7+(unsigned int)1] = ffNE;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffSE))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 < Ny)
						{
							if (idx1==0)
							{
								int gi_high2=getGlobalIdx(1, Ny, idx2); // ghost node
								dist[gi_high2 + DIST_SIZE * 9] = ffSE;
							}
							else
								dist[gi_low + DIST_SIZE * 9+(unsigned int)1] = ffSE;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffTE))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 > 1)
						{
							if (idx2==Nz+1)
							{
								int gi_high2=getGlobalIdx(1, idx1, 1); // ghost node
								dist[gi_high2 + DIST_SIZE * 15] = ffTE;
							}
							else
								dist[gi_low + DIST_SIZE * 15+(unsigned int)1] = ffTE;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffBE))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 < Nz)
						{
							if (idx2==0)
							{
								int gi_high2=getGlobalIdx(1, idx1, Nz); // ghost node
								dist[gi_high2 + DIST_SIZE * 17] = ffBE;
							}
							else
								dist[gi_low + DIST_SIZE * 17+(unsigned int)1] = ffBE;
						}

						else
						{
							if (0){}
						}
					}


				}

				// high to low
			}
		
	}

	// 垂直于y轴的面也是循环边界
	else if (axis == 1)
	{
		#pragma omp parallel for
		for (int idx1=0;idx1<GZ;idx1++)
			for (int idx2=0;idx2<GX;idx2++)
			{
				unsigned int gi_low, gi_high;
				if (idx1 >= Nx+2 || idx2 >= Nz+2)
				{
					continue ;
				}
				gi_low = getGlobalIdx(idx1, 0, idx2); // ghost node
				gi_high = getGlobalIdx(idx1, Ny, idx2); // real node

				{
					// TODO(michalj): Generalize this for grids with e_i > 1.
					// Load distributions to be propagated from low idx to high idx.

					const float ffS = dist[gi_low + DIST_SIZE * 4];
					const float ffSE = dist[gi_low + DIST_SIZE * 9];
					const float ffSW = dist[gi_low + DIST_SIZE * 10];
					const float ffTS = dist[gi_low + DIST_SIZE * 12];
					const float ffBS = dist[gi_low + DIST_SIZE * 14];
					if (gi_high != INVALID_NODE && isfinite(ffS))
					{
						dist[gi_high + DIST_SIZE * 4] = ffS;
					}

					if (isfinite(ffSE))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 > 1)
						{
							dist[gi_high + DIST_SIZE * 9] = ffSE;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffSW))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 < Nx)
						{
							dist[gi_high + DIST_SIZE * 10] = ffSW;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffTS))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 > 1)
						{
							dist[gi_high + DIST_SIZE * 12] = ffTS;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffBS))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 < Nz)
						{
							dist[gi_high + DIST_SIZE * 14] = ffBS;
						}

						else
						{
							if (0){}
						}
					}


				}

				// low to high
				{
					// Load distributrions to be propagated from high idx to low idx.
					const float ffN = dist[gi_high + DIST_SIZE * 3+(unsigned int)GX];
					const float ffNE = dist[gi_high + DIST_SIZE * 7+(unsigned int)GX];
					const float ffNW = dist[gi_high + DIST_SIZE * 8+(unsigned int)GX];
					const float ffTN = dist[gi_high + DIST_SIZE * 11+(unsigned int)GX];
					const float ffBN = dist[gi_high + DIST_SIZE * 13+(unsigned int)GX];
					if (isfinite(ffN) && gi_low != INVALID_NODE)
					{
						dist[gi_low + DIST_SIZE * 3+(unsigned int)GX] = ffN;
					}

					if (isfinite(ffNE))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 > 1)
						{
							dist[gi_low + DIST_SIZE * 7+(unsigned int)GX] = ffNE;
						}

						else
						{
							if (0){}

						}
					}

					if (isfinite(ffNW))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 < Nx)
						{
							dist[gi_low + DIST_SIZE * 8+(unsigned int)GX] = ffNW;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffTN))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 > 1)
						{
							dist[gi_low + DIST_SIZE * 11+(unsigned int)GX] = ffTN;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffBN))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 < Nz)
						{
							dist[gi_low + DIST_SIZE * 13+(unsigned int)GX] = ffBN;
						}

						else
						{
							if (0){}
						}
					}


				}
			}

		// high to low
	}
	// 垂直于z轴的平面
	else
	{
		#pragma omp parallel for
		for (int idx1=0;idx1<GY;idx1++)
			for (int idx2=0;idx2<GX;idx2++)
			{
				unsigned int gi_low, gi_high;
				if (idx1 >= Nx+2 || idx2 >= Ny+2)
				{
					continue ;
				}
				gi_low = getGlobalIdx(idx1, idx2, 0); // ghost node
				gi_high = getGlobalIdx(idx1, idx2, Nz); // real node

				{
					// TODO(michalj): Generalize this for grids with e_i > 1.
					// Load distributions to be propagated from low idx to high idx.

					const float ffB = dist[gi_low + DIST_SIZE * 6];
					const float ffBN = dist[gi_low + DIST_SIZE * 13];
					const float ffBS = dist[gi_low + DIST_SIZE * 14];
					const float ffBE = dist[gi_low + DIST_SIZE * 17];
					const float ffBW = dist[gi_low + DIST_SIZE * 18];
					if (gi_high != INVALID_NODE && isfinite(ffB))
					{
						dist[gi_high + DIST_SIZE * 6] = ffB;
					}

					if (isfinite(ffBN))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 > 1)
						{
							if (idx2==Ny+1)
							{
								int gi_high2=getGlobalIdx(idx1, 1, Nz); // real node
								dist[gi_high2 + DIST_SIZE * 13] = ffBN;
							}
							else
								dist[gi_high + DIST_SIZE * 13] = ffBN;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffBS))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 < Ny)
						{
							if (idx2==0)
							{
								int gi_high2=getGlobalIdx(idx1, Ny, Nz); // real node
								dist[gi_high2 + DIST_SIZE * 14] = ffBS;
							}
							else
								dist[gi_high + DIST_SIZE * 14] = ffBS;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffBE))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 > 1)
						{
							if (idx1==Nx+1)
							{
								int gi_high2=getGlobalIdx(1, idx2, Nz); // real node
								dist[gi_high2 + DIST_SIZE * 17] = ffBE;
							}
							else
								dist[gi_high + DIST_SIZE * 17] = ffBE;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffBW))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 < Nx)
						{
							if (idx1==0)
							{
								int gi_high2=getGlobalIdx(Nx, idx2, Nz); // real node
								dist[gi_high2 + DIST_SIZE * 18] = ffBW;
							}
							else
								dist[gi_high + DIST_SIZE * 18] = ffBW;
						}

						else
						{
							if (0){}
						}
					}


				}

				// low to high
				{
					// Load distributrions to be propagated from high idx to low idx.
					const float ffT = dist[gi_high + DIST_SIZE * 5+(unsigned int)PAGESIZE];
					const float ffTN = dist[gi_high + DIST_SIZE * 11+(unsigned int)PAGESIZE];
					const float ffTS = dist[gi_high + DIST_SIZE * 12+(unsigned int)PAGESIZE];
					const float ffTE = dist[gi_high + DIST_SIZE * 15+(unsigned int)PAGESIZE];
					const float ffTW = dist[gi_high + DIST_SIZE * 16+(unsigned int)PAGESIZE];
					if (isfinite(ffT) && gi_low != INVALID_NODE)
					{
						dist[gi_low + DIST_SIZE * 5+(unsigned int)PAGESIZE] = ffT;
					}

					if (isfinite(ffTN))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 > 1)
						{
							if (idx2==Ny+1)
							{
								int gi_high2=getGlobalIdx(idx1, 1, 1); // real node
								dist[gi_high2 + DIST_SIZE * 11] = ffTN;
							}
							else
								dist[gi_low + DIST_SIZE * 11+(unsigned int)PAGESIZE] = ffTN;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffTS))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx2 < Ny)
						{
							if (idx2==0)
							{
								int gi_high2=getGlobalIdx(idx1, Ny, 1); // real node
								dist[gi_high2 + DIST_SIZE * 12] = ffTS;
							}
							else
								dist[gi_low + DIST_SIZE * 12+(unsigned int)PAGESIZE] = ffTS;
						}

						else
						{
							if (0){}
						}
					}

					if (isfinite(ffTE))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 > 1)
						{
							if (idx1==Nx+1)
							{
								int gi_high2=getGlobalIdx(1, idx2, 1); // real node
								dist[gi_high2 + DIST_SIZE * 15] = ffTE;
							}
							else
								dist[gi_low + DIST_SIZE * 15+(unsigned int)PAGESIZE] = ffTE;
						}

						else
						{
							if (0){}

						}
					}

					if (isfinite(ffTW))
					{
						// Skip distributions which are not populated or cross multiple boundaries.
						if (idx1 < Nx)
						{
							if (idx1==0)
							{
								int gi_high2=getGlobalIdx(Nx, idx2, 1); // real node
								dist[gi_high2 + DIST_SIZE * 16] = ffTW;
							}
							else
								dist[gi_low + DIST_SIZE * 16+(unsigned int)PAGESIZE] = ffTW;
						}

						else
						{
							if (0){}

						}
					}


				}

				// high to low
			}

			}
		//printf("zzzzzzzzzzzzzzzzzzzzzzzzzzzz\n");
		
}

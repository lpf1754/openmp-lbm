// 初始化颗粒 位置，速度，大小
//__global__ void InitParticle(float* size,float *pos,float *vel, float *force);
// 计算颗粒受力，EPS，碰撞项
//__global__ void ParticleKernel(float* size,float *pos, float* vel,float *force,float* d_u,float* d_v,float *dw,float* __restrict__ d_rho,float *dist_in,float* dist_s,float * BS);

void InitParticle(float* size,float *pos,float *vel, float *force,float *particle_mass,float *IB);

// // 计算颗粒受到的水动力，EPS，碰撞项
void ParticleForce(float* size,float *pos, float* vel,float *force,float* d_u,float* d_v,float* d_w,
	float*  d_rho,float *dist_in,float* dist_s,float * BS,float* pBS,float* u_tmp,float* v_tmp,float* w_tmp);
// 计算颗粒间作用力，更新颗粒速度和位置
 void UpdateParticle(float* size,float *pos, float* vel,float *force,float *particle_mass,float *IB);

 // 计算颗粒的临时速度tmp
 void ParticleVelTmp(float* size,float *pos, float* vel,float* u_tmp,float* v_tmp,float* w_tmp,float * BS,float *pBS);
 // 计算颗粒的EPS(固体分数)
 void ParticleEps(float* size,float *pos,float * BS,float* pBS);
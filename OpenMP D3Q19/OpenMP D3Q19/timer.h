#include <Windows.h>  //用于计时
#include <iostream>
using namespace std; 
class Timer
{
private:
	LARGE_INTEGER timeStart;    //开始时间  
	LARGE_INTEGER timeEnd;      //结束时间  
	LARGE_INTEGER frequency;    //计时器频率  
	float quadpart;
public:
	void starttimer()
	{
		QueryPerformanceFrequency(&frequency);  
		quadpart = (float)frequency.QuadPart;//计时器频率  
		QueryPerformanceCounter(&timeStart);  
	}

	void stoptimer(int output)
	{
		QueryPerformanceCounter(&timeEnd);  
		//得到两个时间的耗时  
		float elapsed = (timeEnd.QuadPart - timeStart.QuadPart) / quadpart;  
		//if (output)
		std::cout << "TIME   "<<elapsed << std::endl;//单位为秒，精度为微秒(1000000/cpu主频)  
	}

};

#include "params.h"
#include <cmath>
#include <stdio.h>
#include <iostream>
using namespace std;
//                        0  1    2    3     4    5    6   7   8    9  10  11 12 13 14 15 16 17 18 
//                        C  E    W    N     S    T    B   NE  NW   SE SW  TN TS BN BS TE TW BE BW
const int cx[Q] = {0, 1 , -1 ,  0  ,  0  , 0 ,  0 , 1 , -1,  1, -1, 0,  0, 0, 0, 1,-1, 1,-1}; // lattice weights
const int cy[Q] = {0, 0 ,  0 ,  1  , -1 ,  0 ,  0,  1 ,  1, -1, -1, 1, -1, 1,-1, 0, 0, 0, 0}; // lattice weights
const int cz[Q] = {0, 0 ,  0 ,  0  ,  0  , 1 , -1,  0 ,  0 , 0,  0, 1,  1,-1,-1, 1, 1,-1,-1}; // lattice weights
const int op[Q]=  {0, 2,   1,   4,    3,   6,   5, 10,   9, 8,   7, 14,13,12,11,18,17,16,15};
//const float weight[9] = {4./9., 1./9., 1./9., 1./9., 1./9., 1./36., 1./36., 1./36., 1./36.}; // lattice weights
#ifndef MAX
#define MAX(a,b) (a > b ? a : b)
#endif
#ifndef MIN
#define MIN(a,b) (a > b ? b : a)
#endif
#define SQ(a) pow(a,2)  //定义宏必须非常小心， SQ(a) (a*a) 当 a=b+c 就会变成 SQ(a)= b+c*b+c 完全不是平方的原意 

// Dist 代表distribution（分布），一共九个分布函数，用来存储一个节点的分布函数
typedef struct Dist
{
	float fC;
	float fE;
	float fW;
	float fN;
	float fS;
	float fT;
	float fB;
	float fNE;
	float fNW;
	float fSE;
	float fSW;
	float fTN;
	float fTS;
	float fBN;
	float fBS;
	float fTE;
	float fTW;
	float fBE;
	float fBW;
}
Dist;

// 计算各个格子的固体质量分数
#define LR 0.8660254037844386f
inline float compute_eps(float *r0,float r,float xi,float yi,float zi)
{
	float X=xi;float Y=yi;float Z=zi;
	float eps=0.0f;
	float distx=r0[0]-X;
	float disty=r0[1]-Y;
	float distz=r0[2]-Z;
	float dist2=distx*distx+disty*disty+distz*distz;
	if (dist2>SQ(LR+r))
		return eps; //完全在颗粒外，eps=0
	if (dist2<SQ(r-LR)) //完全在颗粒内部 eps=1
	{
		return 1.0f;
	} 
	// 跨边界的格子
	int m=10;float dl=1.0f/float(m) ;float dl5=0.5*dl;// 将格子分成50x50个小格子,每个格子边长dl
	float all=(float)m*m*m;
	float ct=0; //计数器
	float r2=r*r;
	float x0=X-0.5;float y0=Y-0.5;float z0=	Z-0.5; //格子的左下角坐标
	for(int k=0;k<m;k++)
		for(int i=0;i<m;i++)
			for (int j=0;j<m;j++)
			{
				if (SQ(x0+i*dl+dl5-r0[0])+SQ(y0+j*dl+dl5-r0[1])+SQ(z0+k*dl+dl5-r0[2])<r2) //在圆内部
				{
					ct+=1; //计数器加一
				}
			}
			eps=ct/all;
			return eps;
}

// 初始化颗粒
void InitParticle(float* size,float *pos,float *vel, float *force,float *particle_mass,float *IB)
{   
	for(int i=0;i<PARTICLE_NUM;i++)
	{
		// 初始位置
		pos[i+0*PARTICLE_NUM]=Nx/2.0f+(i%PARTICLE_NUM_X)*7.200f; //间隔15布置一个颗粒
		pos[i+1*PARTICLE_NUM]=0.5f+Ny/2.0f ;//+(i/PARTICLE_NUM_X)*20.0f; //间隔15布置一个颗粒
		pos[i+2*PARTICLE_NUM]=0.5f+Nz/2.0f ;//+(i/PARTICLE_NUM_X)*20.0f; //间隔15布置一个颗粒
		// 初始大小
		size[i]=3.5f; //直径最大是7
		// 初始化速度(三个线性，3个转动）
		vel[i+0*PARTICLE_NUM]=0.001f;
		vel[i+1*PARTICLE_NUM]=0.0f;
		vel[i+2*PARTICLE_NUM]=0.0f;
		vel[i+3*PARTICLE_NUM]=0.0f;
		vel[i+4*PARTICLE_NUM]=0.0f;
		vel[i+5*PARTICLE_NUM]=0.0f;
		// 初始化力 (三个线性，3个力矩）
		force[i+0*PARTICLE_NUM]=0.0f;
		force[i+1*PARTICLE_NUM]=0.0f;
		force[i+2*PARTICLE_NUM]=0.0f;
		force[i+3*PARTICLE_NUM]=0.0f;
		force[i+4*PARTICLE_NUM]=0.0f;
		force[i+5*PARTICLE_NUM]=0.0f;

		// 初始化颗粒的质量
		particle_mass[i]=3.1415926f*4.0f/3.0f*size[i]*size[i]*size[i];//质量
		IB[i]=2.0/5*particle_mass[i]*size[i]*size[i];//转动惯量
	}
	
}


// 设备函数 计算平衡分布
inline void equilibrium( float den, float vel_x, float vel_y,float vel_z,float* feq0)
{
	feq0[0] = (1.0f *(1.0f / 3.0f)) *den *( - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_x
		*vel_x) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_y *vel_y) - 3.0f *(1.0f *(1.0f /
		2.0f))*(vel_z *vel_z)) + (1.0f *(1.0f / 3.0f)) *den;
	feq0[1] = (1.0f *(1.0f / 18.0f)) *den *(vel_x*(3.0f *vel_x + 3.0f) - 3.0f *
		(1.0f *(1.0f / 2.0f))*(vel_y *vel_y) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_z
		*vel_z)) + (1.0f *(1.0f / 18.0f)) *den;
	feq0[2] = (1.0f *(1.0f / 18.0f)) *den *(vel_x*(3.0f *vel_x - 3.0f) - 3.0f *
		(1.0f *(1.0f / 2.0f))*(vel_y *vel_y) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_z
		*vel_z)) + (1.0f *(1.0f / 18.0f)) *den;
	feq0[3] = (1.0f *(1.0f / 18.0f)) *den *( - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_x
		*vel_x) + vel_y*(3.0f *vel_y + 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_z
		*vel_z)) + (1.0f *(1.0f / 18.0f)) *den;
	feq0[4] = (1.0f *(1.0f / 18.0f)) *den *( - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_x
		*vel_x) + vel_y*(3.0f *vel_y - 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_z
		*vel_z)) + (1.0f *(1.0f / 18.0f)) *den;
	feq0[5] = (1.0f *(1.0f / 18.0f)) *den *( - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_x
		*vel_x) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_y *vel_y) + vel_z*(3.0f *vel_z +
		3.0f)) + (1.0f *(1.0f / 18.0f)) *den;
	feq0[6] = (1.0f *(1.0f / 18.0f)) *den *( - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_x
		*vel_x) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_y *vel_y) + vel_z*(3.0f *vel_z -
		3.0f)) + (1.0f *(1.0f / 18.0f)) *den;
	feq0[7] = (1.0f *(1.0f / 36.0f)) *den *(vel_x*(3.0f *vel_x + 9.0f * vel_y +
		3.0f) + vel_y*(3.0f *vel_y + 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_z
		*vel_z)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[8] = (1.0f *(1.0f / 36.0f)) *den *(vel_x*(3.0f *vel_x - 9.0f * vel_y -
		3.0f) + vel_y*(3.0f *vel_y + 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_z
		*vel_z)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[9] = (1.0f *(1.0f / 36.0f)) *den *(vel_x*(3.0f *vel_x - 9.0f * vel_y +
		3.0f) + vel_y*(3.0f *vel_y - 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_z
		*vel_z)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[10] = (1.0f *(1.0f / 36.0f)) *den *(vel_x*(3.0f *vel_x + 9.0f * vel_y -
		3.0f) + vel_y*(3.0f *vel_y - 3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_z
		*vel_z)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[11] = (1.0f *(1.0f / 36.0f)) *den *( - 3.0f *(1.0f *(1.0f / 2.0f))*
		(vel_x *vel_x) + vel_y*(3.0f *vel_y + 9.0f * vel_z + 3.0f) + vel_z*(3.0f
		*vel_z + 3.0f)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[12] = (1.0f *(1.0f / 36.0f)) *den *( - 3.0f *(1.0f *(1.0f / 2.0f))*
		(vel_x *vel_x) + vel_y*(3.0f *vel_y - 9.0f * vel_z - 3.0f) + vel_z*(3.0f
		*vel_z + 3.0f)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[13] = (1.0f *(1.0f / 36.0f)) *den *( - 3.0f *(1.0f *(1.0f / 2.0f))*
		(vel_x *vel_x) + vel_y*(3.0f *vel_y - 9.0f * vel_z + 3.0f) + vel_z*(3.0f
		*vel_z - 3.0f)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[14] = (1.0f *(1.0f / 36.0f)) *den *( - 3.0f *(1.0f *(1.0f / 2.0f))*
		(vel_x *vel_x) + vel_y*(3.0f *vel_y + 9.0f * vel_z - 3.0f) + vel_z*(3.0f
		*vel_z - 3.0f)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[15] = (1.0f *(1.0f / 36.0f)) *den *(vel_x*(3.0f *vel_x + 9.0f * vel_z +
		3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_y *vel_y) + vel_z*(3.0f *vel_z +
		3.0f)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[16] = (1.0f *(1.0f / 36.0f)) *den *(vel_x*(3.0f *vel_x - 9.0f * vel_z -
		3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_y *vel_y) + vel_z*(3.0f *vel_z +
		3.0f)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[17] = (1.0f *(1.0f / 36.0f)) *den *(vel_x*(3.0f *vel_x - 9.0f * vel_z +
		3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_y *vel_y) + vel_z*(3.0f *vel_z -
		3.0f)) + (1.0f *(1.0f / 36.0f)) *den;
	feq0[18] = (1.0f *(1.0f / 36.0f)) *den *(vel_x*(3.0f *vel_x + 9.0f * vel_z -
		3.0f) - 3.0f *(1.0f *(1.0f / 2.0f))*(vel_y *vel_y) + vel_z*(3.0f *vel_z -
		3.0f)) + (1.0f *(1.0f / 36.0f)) *den;

}


// 计算颗粒的EPS(固体分数)
void ParticleEps(float* size,float *pos,float * BS,float* pBS)
{   
	for(int pid=0;pid<PARTICLE_NUM;pid++)
	{
		
		// 共享内存，存储颗粒的位置，速度等公共量
		float xy[3];  //位置
		float pr[1];    // 颗粒半径 
		xy[0]=pos[pid];xy[1]=pos[pid+PARTICLE_NUM];xy[2]=pos[pid+2*PARTICLE_NUM];//x,Y
		pr[0]=size[pid];
#pragma omp parallel for
		for(int lx=0;lx<EDGE;lx++)
			for(int ly=0;ly<EDGE;ly++)
				for(int lz=0;lz<EDGE;lz++)
				{
					//（1）  计算当前线程对应的格点坐标 =最低点+局部偏移
					int gx=int(xy[0]-pr[0]+Nx)-Nx+lx; //x方向是循环的，会出现负值
					int gy=int(xy[1]-pr[0])+ly;
					int gz=int(xy[2]-pr[0])+lz;
					if (gx<1) gx=gx+Nx;  // 0是ghost层
					if (gx>Nx) gx=gx-Nx; // nx+1 也是ghost层
					int gi=gz*PAGESIZE+gy*GX+gx;
					int lgi=lz*PPAGESIZE+ly*EDGE+lx;  // 线程中一位共享内存数组中的编号
					//（2）计算当前格点的eps
					float B=compute_eps(xy,pr[0],(float)gx,(float)gy,(float)gz);  //caleps : 快速但是不够精确； compute_eps：更精确一点，但是很慢，应该优先使用 caleps
					// (3) 如果B> 计算固体和流体平衡函数;
					B=B*(TAU-0.5)/((1-B)+(TAU-0.5));  // 不要忘了这一步,修改后与cpu版本一致
					pBS[pid*PVOLUMESIZE+lgi]=B;
					//if(lx<=1 && pid==1)
					//printf("%d %d %f\n",gi,lx,B);
					BS[gi]+=B;  //更新格点eps
				}
	}

}

// 计算颗粒的临时速度tmp
void ParticleVelTmp(float* size,float *pos, float* vel,float* u_tmp,float* v_tmp,float* w_tmp,float * BS,float *pBS)
{   //FILE* fn=fopen("out.txt","w");
	for(int pid=0;pid<PARTICLE_NUM;pid++)
	{
		//
		
		// 共享内存，存储颗粒的位置，速度等公共量
		float xy[3];  //位置
		float pvel[6];  //线速度和角速度
		float pr[1];    // 颗粒半径 
		xy[0]=pos[pid];xy[1]=pos[pid+PARTICLE_NUM];xy[2]=pos[pid+2*PARTICLE_NUM];//x,Y
		for (int k=0;k<6;k++)
		{
			pvel[k]=vel[pid+k*PARTICLE_NUM];
		}
		pr[0]=size[pid];
		//printf("#################### %d %f\n",pid,xy[0]);
#pragma omp parallel for
		for(int lx=0;lx<EDGE;lx++)
			for(int ly=0;ly<EDGE;ly++)
				for(int lz=0;lz<EDGE;lz++)
				{
					//（1）  计算当前线程对应的格点坐标 =最低点+局部偏移
					int gx=int(xy[0]-pr[0]+Nx)-Nx+lx; //x方向是循环的，会出现负值
					int gy=int(xy[1]-pr[0])+ly;
					int gz=int(xy[2]-pr[0])+lz;
					if (gx<1) gx=gx+Nx;  // 0是ghost层
					if (gx>Nx) gx=gx-Nx; // nx+1 也是ghost层
					float fx=0,fy=0,fz=0; //初始化力
					float tx=0,ty=0,tz=0; //初始化力
					int gi=gz*PAGESIZE+gy*GX+gx;
					int lgi=lz*PPAGESIZE+ly*EDGE+lx;  // 线程中一位共享内存数组中的编号

					//（2）计算当前格点的固体速度
					float B_total=BS[gi];
					float B_me=pBS[pid*PVOLUMESIZE+lgi];
					//printf("%d %f %f %f %f \n",pid,B_me/B_total,B_me,B_total,xy[0]);
					
					// (3) 如果B> 计算固体和流体平衡函数
					if (B_total>0)
					{
						
						//if(B_me/B_total<1)
						//fprintf(fn,"%d %d %d %f %f %f %f \n",pid,gi,lx,B_me,B_total,B_me/B_total,xy[0]);
				    	// 首先计算固体点速度
						float dx=(float)gx-xy[0]; //当前格点的力臂
						float dy=(float)gy-xy[1];
						float dz=(float)gz-xy[2];
						float vs[3];
						vs[0]=pvel[0]+pvel[3]*dz-pvel[5]*dy;
						vs[1]=pvel[1]+pvel[5]*dx-pvel[3]*dz;
						vs[2]=pvel[2]+pvel[3]*dy-pvel[4]*dx;
						//速度加权
						u_tmp[gi]+=B_me/B_total*vs[0];
						v_tmp[gi]+=B_me/B_total*vs[1];
						w_tmp[gi]+=B_me/B_total*vs[2];
						/*u_tmp[gi]=vs[0];
						v_tmp[gi]=vs[1];
						w_tmp[gi]=vs[2];*/
					}
				}
	}

	//fclose(fn);
}


// 这个内核计算颗粒计算每个格点的eps,固体碰撞项，颗粒受力
// 每个BLOCK处理一个颗粒
//颗粒大小 size,颗粒位置 pos, 颗粒速度 vel,颗粒受力 force,流体速度 d_u,流体速度 d_v,流体密度 d_rho, 流体分布函数 dist_in ,固体碰撞分布dist_s
#define PBLOCK BLOCKSIZE*8
void ParticleForce(float* size,float *pos, float* vel,float *force,float* d_u,float* d_v,float* d_w,
	float*  d_rho,float *dist_in,float* dist_s,float * BS,float* pBS,float* u_tmp,float* v_tmp,float* w_tmp)
{   
	for(int pid=0;pid<PARTICLE_NUM;pid++)
	{
		// 共享内存，存储颗粒的位置，速度等公共量
		float xy[3];  //位置
		float pvel[6];  //线速度和角速度
		float pr[1];    // 颗粒半径 
		float fsum0=0,fsum1=0,fsum2=0,fsum3=0,fsum4=0,fsum5=0;
		xy[0]=pos[pid];xy[1]=pos[pid+PARTICLE_NUM];xy[2]=pos[pid+2*PARTICLE_NUM];//x,Y
		for (int k=0;k<6;k++)
		{
			pvel[k]=vel[pid+k*PARTICLE_NUM];
		}
		pr[0]=size[pid];
		#pragma omp parallel for reduction(+: fsum0,fsum1,fsum2,fsum3,fsum4,fsum5)
		for(int lx=0;lx<EDGE;lx++)
			for(int ly=0;ly<EDGE;ly++)
				for(int lz=0;lz<EDGE;lz++)
				{
					//（1）  计算当前线程对应的格点坐标 =最低点+局部偏移
					int gx=int(xy[0]-pr[0]+Nx)-Nx+lx; //x方向是循环的，会出现负值
					int gy=int(xy[1]-pr[0])+ly;
					int gz=int(xy[2]-pr[0])+lz;
					if (gx<1) gx=gx+Nx;  // 0是ghost层
					if (gx>Nx) gx=gx-Nx; // nx+1 也是ghost层
					float fx=0,fy=0,fz=0; //初始化力
					float tx=0,ty=0,tz=0; //初始化力
					int gi=gz*PAGESIZE+gy*GX+gx;
					int lgi=lz*PPAGESIZE+ly*EDGE+lx;  // 线程中一位共享内存数组中的编号
					
					//（2）计算当前格点的eps
					float B_total=BS[gi];
					float dx=0;float dy=0; float dz=0;//当前格点的力臂
					// (3) 如果B> 计算固体和流体平衡函数
					if (B_total>0)
					{
						//printf("%d %d  %f\n",gi,lgi,B_total);
						float B_me=pBS[pid*PVOLUMESIZE+lgi];
						float fx=0,fy=0,fz=0; //初始化力
						float tx=0,ty=0,tz=0; //初始化力
						// 首先计算固体点速度
						dx=(float)gx-xy[0];dy=(float)gy-xy[1];dz=(float)gz-xy[2];
						float vs[3];
						vs[0]=u_tmp[gi];
						vs[1]=v_tmp[gi];
						vs[2]=w_tmp[gi];
						float fl[Q],fs[Q];
						//printf("%d %d  %f %f %f %f\n",gi,lgi,B_total,vs[0],vs[1],vs[2]);
						equilibrium(d_rho[gi],vs[0],vs[1],vs[2],fs); //固体平衡分布
						vs[0]=d_u[gi];vs[1]=d_v[gi];vs[2]=d_w[gi];
						equilibrium(d_rho[gi],vs[0],vs[1],vs[2],fl); //流体平衡分布
						// (4) 计算固体碰撞项
						// f0
						float f=B_total*(dist_in[gi]-fl[0]+fs[0]-dist_in[gi]);
						//printf("%d %d  %f %f %f %f %f\n",gi,lgi,B_total,vs[0],vs[1],vs[2],f);
						dist_s[gi]=f;
						for (int k=1;k<Q;k++)
						{
							float f=B_total*(dist_in[gi+op[k]*DIST_SIZE]-fl[op[k]]+fs[k]-dist_in[gi+k*DIST_SIZE]);
							fx+=cx[k]*f*B_me/B_total;
							fy+=cy[k]*f*B_me/B_total;
							fz+=cz[k]*f*B_me/B_total;
							dist_s[gi+k*DIST_SIZE]=f;
						}
						fsum0+=-fx;//(float*)&
						fsum1+=-fy;
						fsum2+=-fz;
						fsum3+=-(fz*dy-fy*dz);//(float*)&
						fsum4+=-(fx*dz-fz*dx);
						fsum5+=-(fy*dx-fx*dy);
					}
				}
				force[pid+0*PARTICLE_NUM] =fsum0;
				force[pid+1*PARTICLE_NUM] =fsum1;
				force[pid+2*PARTICLE_NUM] =fsum2;
				force[pid+3*PARTICLE_NUM] =fsum3;
				force[pid+4*PARTICLE_NUM] = fsum4;
				force[pid+5*PARTICLE_NUM] = fsum5;
	}


}

// 计算颗粒间的范德华力
inline float van_der_waals(float ai,float aj,float rij)
{
	// ai，aj: 颗粒i,j的半径；rij: 颗粒ij的圆心间的距离
	// 哈梅克常数
	float A=1e-20;
	// 长度无量纲转换因子
	float Cl=1e-8;
	// 时间无量纲转换因子
	float nu=(TAU-0.5)/3;//粘度
	float Ct=nu*Cl*Cl/1e-6;
	//cout<<"CT "<<Ct<<endl;
	//getchar();
	// 密度无量纲转化因子
	float Cr=1000.0f;
	// 首先，计算有量纲的作用力
	ai=ai*Cl;aj=aj*Cl;rij=rij*Cl;  //转为有量纲
	//float f=(-32*A*pow(ai,3)*pow(aj,3)*rij)/
	//	(3.*pow(pow(ai,2) - 2*ai*aj + pow(aj,2) - pow(rij,2),2)
	//	*pow(pow(ai,2) + 2*ai*aj + pow(aj,2) - pow(rij,2),2));
	// 或者，用简化的公式
	float h=rij-ai-aj;
	float f=-A*ai*aj/(6*h*h*(ai+aj));
	// 然后，将力转到格子单位
	f=f/(Cr*Cl*Cl*Cl*Cl/Ct/Ct);
	return f;
}

// 计算颗粒之间静电作用力（dlvo理论）
 inline float DLVO(float ai,float aj,float rij)
{
	// ai，aj: 颗粒i,j的半径；rij: 颗粒ij的圆心间的距离
	// 哈梅克常数
	float er=78.24;
	// 绝对温度
	float T=298.15;
	// （氯化钠）溶液的摩尔浓度,摩尔/升
	float n=1.6;
	// 颗粒表面电位
	float xi=-34.9e-3;
	// 长度无量纲转换因子
	float Cl=1e-8;
	// 时间无量纲转换因子
	float nu=(TAU-0.5)/3;//粘度
	float Ct=nu*Cl*Cl/1e-6;
	// 密度无量纲转化因子
	float Cr=1000.0f;
	// 首先，计算有量纲的作用力
	ai=ai*Cl;aj=aj*Cl;rij=rij*Cl;  //转为有量纲
	// 计算Debye长度的倒数k
	float k=5.027838127937603e11*sqrt(n/(T*er));
	// 计算gama1,gama2
	float gama1=tanh((2900.0724112961616*xi)/T);
	// 受力计算
	float h=rij-ai-aj;
	float f=(3.344215628242738e6*n*T*(ai*aj/(ai+aj))*gama1*gama1)/(exp(h*k)*k);
	// 然后，将力转到格子单位
	f=f/(Cr*Cl*Cl*Cl*Cl/Ct/Ct);
	return f;
}

// 玻恩斥力
 inline float born_repulsion(float ai,float aj,float rij)
{
	// ai，aj: 颗粒i,j的半径；rij: 颗粒ij的圆心间的距离
	// sigma
	float sigma=0.5e-9;
	// 哈梅克常数
	float A=1e-20;
	// 长度无量纲转换因子
	float Cl=1e-8;
	// 时间无量纲转换因子
	float nu=(TAU-0.5)/3;//粘度
	float Ct=nu*Cl*Cl/1e-6;
	// 密度无量纲转化因子
	float Cr=1000.0f;
	// 首先，计算有量纲的作用力
	ai=ai*Cl;aj=aj*Cl;rij=rij*Cl;  //转为有量纲
	double h=rij-ai-aj; // h一定要设置成双精度浮点类型，不然会出错
	float f=-(A*(-pow(h,-7) + pow(h + 2*ai,-7) - (7*(-h + 6*ai))/pow(h,8) - (7*(h + 8*ai))/pow(h + 2*ai,8))*1.5625e-56)/7560.;
	f=f/(Cr*Cl*Cl*Cl*Cl/Ct/Ct);
	return f;
}

// 计算颗粒间作用力，更新颗粒速度和位置
#define VERLET
void UpdateParticle(float* size,float *pos, float* vel,float *force,float *particle_mass,float *IB)
{
	// 共享内存，存储颗粒的位置，速度等公共量
	float* xy=pos;  //位置
	float* v=vel;  //线速度和角速度
	float* r=size;    // 颗粒半径 
	float* f=force;  //颗粒的水动力
	// 使用速度verlet积分
#ifdef VERLET
	double* force_tmp=new double[PARTICLE_NUM*6];
	double* force_tmp2=new double[PARTICLE_NUM*6];
	int tn=10;
	double dt=1.0/(double)tn;
	for (int t=0;t<tn;t++)
	{
		for (int pid=0;pid<PARTICLE_NUM;pid++)
		{
			for(int i=0;i<6;i++)
			force_tmp[pid+i*PARTICLE_NUM]=force[pid+i*PARTICLE_NUM];
		}
		
		//（1） 计算t时刻颗粒间作用力 
		for(int pid=0;pid<PARTICLE_NUM;pid++)
			for (int i=0;i<PARTICLE_NUM;i++)
			{
				if(pid==i) continue; //自身
				float dx=pos[pid]-pos[i];
				float dy=pos[pid+PARTICLE_NUM]-pos[i+PARTICLE_NUM];
				float dz=pos[pid+PARTICLE_NUM]-pos[i+2*PARTICLE_NUM];
				float dist2=dx*dx+dy*dy+dz*dz;
				float dist=sqrt(dist2);
				float rij=r[pid]+r[i];
				float h=dist-rij;
				// 待修改
				//float tf=ap*0.6*((32768*pow(r[pid],14))/pow(dist,14) - (256*pow(r[pid],8))/pow(dist,8))*dist;
				// 范德华力+静电力+玻恩斥力
				float vd=van_der_waals(r[pid],r[i],dist);
				float dl=DLVO(r[pid],r[i],dist);
				float br=born_repulsion(r[pid],r[i],dist);
				//printf("h %f vdw %f   dlvo %f   born  %f  total %f \n",h,vd,dl,br,vd+dl+br);
				float tf=vd+dl+br;
				force_tmp[pid]+=tf*dx/dist;
				force_tmp[pid+PARTICLE_NUM]+=tf*dy/dist;
				force_tmp[pid+2*PARTICLE_NUM]+=tf*dz/dist;
			}
		//（2）计算t+1时刻颗粒的位置
		for (int pid=0;pid<PARTICLE_NUM;pid++)
		{
			//cout<<"vel "<<v[pid]<<"  force "<<force_tmp[pid]<<" pos "<<pos[pid]<<endl;
			
			pos[pid]+=v[pid]*dt+0.5*force_tmp[pid]/particle_mass[pid]*dt*dt;
			pos[pid+PARTICLE_NUM]+=v[pid+PARTICLE_NUM]*dt+0.5*force_tmp[pid+PARTICLE_NUM]/particle_mass[pid]*dt*dt;
			pos[pid+2*PARTICLE_NUM]+=v[pid+2*PARTICLE_NUM]*dt+0.5*force_tmp[pid+2*PARTICLE_NUM]/particle_mass[pid]*dt*dt;
		}
		//(3) 计算t+1时刻的力
		for (int pid=0;pid<PARTICLE_NUM;pid++)
		{
			for(int i=0;i<6;i++)
				force_tmp2[pid+i*PARTICLE_NUM]=force[pid+i*PARTICLE_NUM];
		}
		for(int pid=0;pid<PARTICLE_NUM;pid++)
			for (int i=0;i<PARTICLE_NUM;i++)
			{
				if(pid==i) continue; //自身
				float dx=pos[pid]-pos[i];
				float dy=pos[pid+PARTICLE_NUM]-pos[i+PARTICLE_NUM];
				float dz=pos[pid+PARTICLE_NUM]-pos[i+2*PARTICLE_NUM];
				float dist2=dx*dx+dy*dy+dz*dz;
				float dist=sqrt(dist2);
				float rij=r[pid]+r[i];
				float h=dist-rij;
				// 待修改
				//float tf=ap*0.6*((32768*pow(r[pid],14))/pow(dist,14) - (256*pow(r[pid],8))/pow(dist,8))*dist;
				// 范德华力+静电力+玻恩斥力
				float vd=van_der_waals(r[pid],r[i],dist);
				float dl=DLVO(r[pid],r[i],dist);
				float br=born_repulsion(r[pid],r[i],dist);
				//printf("h %f vdw %f   dlvo %f   born  %f  total %f \n",h,vd,dl,br,vd+dl+br);
				float tf=vd+dl+br;
				force_tmp2[pid]+=tf*dx/dist;
				force_tmp2[pid+PARTICLE_NUM]+=tf*dy/dist;
				force_tmp2[pid+2*PARTICLE_NUM]+=tf*dz/dist;
				
			}
		// (4) 计算t+1时刻的速度
			for (int pid=0;pid<PARTICLE_NUM;pid++)
			{
				v[pid]+=0.5*(force_tmp[pid]+force_tmp2[pid])*dt/particle_mass[pid];
				v[pid+PARTICLE_NUM]+=0.5*(force_tmp[pid+PARTICLE_NUM]+force_tmp2[pid+PARTICLE_NUM])*dt/particle_mass[pid];
				v[pid+2*PARTICLE_NUM]+=0.5*(force_tmp[pid+2*PARTICLE_NUM]+force_tmp2[pid+2*PARTICLE_NUM])*dt/particle_mass[pid];
				v[pid+3*PARTICLE_NUM]+=dt*f[pid+3*PARTICLE_NUM]/IB[pid];
				v[pid+4*PARTICLE_NUM]+=dt*f[pid+4*PARTICLE_NUM]/IB[pid];
				v[pid+5*PARTICLE_NUM]+=dt*f[pid+5*PARTICLE_NUM]/IB[pid];
			}
	}
#else
	//欧拉积分
	//// 计算颗粒间作用力
	for(int pid=0;pid<PARTICLE_NUM;pid++)
	for (int i=0;i<PARTICLE_NUM;i++)
	{
		if(pid==i) continue; //自身
		float dx=xy[pid]-xy[i];
		float dy=xy[pid+PARTICLE_NUM]-xy[i+PARTICLE_NUM];
		float dz=xy[pid+PARTICLE_NUM]-xy[i+2*PARTICLE_NUM];
		float dist2=dx*dx+dy*dy+dz*dz;
		float dist=sqrt(dist2);
		float rij=r[pid]+r[i];
		float h=dist-rij;
		if (1)
		{
			// 待修改
			//float tf=ap*0.6*((32768*pow(r[pid],14))/pow(dist,14) - (256*pow(r[pid],8))/pow(dist,8))*dist;
			// 范德华力+静电力+玻恩斥力
			float vd=van_der_waals(r[pid],r[i],dist);
			float dl=DLVO(r[pid],r[i],dist);
			float br=born_repulsion(r[pid],r[i],dist);
			//printf("h %f vdw %f   dlvo %f   born  %f  total %f \n",h,vd,dl,br,vd+dl+br);
			float tf=vd+dl+br;
			f[pid]+=tf*dx/dist;
			f[pid+PARTICLE_NUM]+=tf*dy/dist;
			f[pid+2*PARTICLE_NUM]+=tf*dz/dist;
		}
	}
	
	for(int pid=0;pid<PARTICLE_NUM;pid++)
	{
		// 更新颗粒位置
		pos[pid]+=v[pid]+f[pid]/particle_mass[pid]*0.5;
		pos[pid+PARTICLE_NUM]+=v[pid+PARTICLE_NUM]+f[pid+1*PARTICLE_NUM]/particle_mass[pid]*0.5;
		pos[pid+2*PARTICLE_NUM]+=v[pid+2*PARTICLE_NUM]+f[pid+2*PARTICLE_NUM]/particle_mass[pid]*0.5;
		// 更新颗粒速度
		v[pid]+=f[pid]/particle_mass[pid];
		v[pid+PARTICLE_NUM]+=f[pid+PARTICLE_NUM]/particle_mass[pid];
		v[pid+2*PARTICLE_NUM]+=f[pid+2*PARTICLE_NUM]/particle_mass[pid];
		v[pid+3*PARTICLE_NUM]+=f[pid+3*PARTICLE_NUM]/IB[pid];
		v[pid+4*PARTICLE_NUM]+=f[pid+4*PARTICLE_NUM]/IB[pid];
		v[pid+5*PARTICLE_NUM]+=f[pid+5*PARTICLE_NUM]/IB[pid];
	}
#endif
	getchar();
}
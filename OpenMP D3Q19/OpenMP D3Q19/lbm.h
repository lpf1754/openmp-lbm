// A kernel to set the node distributions using the equilibrium distributions
// and the macroscopic fields. 内核,初始化分布函数
void SetInitialConditions(float *dist1_in, float * ivx,
	float * ivy, float * ivz, const float *
	irho, const int * map);

// 内涵 预处理宏观量(密度)
void PrepareMacroFields(float *orho, float *u,float *v ,float* w,int *map,float* BS,float*u_tmp,float*v_tmp,float*w_tmp);
//内核,碰撞和迁移  //sailfish支持AA模式，但是这里是AB模式，不要被误导了
//__global__ void CollideAndPropagate(const int * map,float * dist_in,float * dist_out,float * gg0m0,float * ovx,float * ovy,  int options );
void CollideAndPropagate(const int * map,float * dist_in,float * dist_out,float * gg0m0,
	float * ovx,float * ovy,float * ovz, float *BS,float *dist_s, int options,float*u_tmp,float*v_tmp,float*w_tmp );
// Applies periodic boundary conditions within a single subdomain.
//  dist: pointer to the distributions array
//  axis: along which axis the PBCs are to be applied (0:x, 1:y, 2:z)
 void ApplyPeriodicBoundaryConditions(float *dist, int axis);

//__global__ void CalEPS();

//计算宏观量
 void CalVelandRho(float *  din,float * d_rho,float * du,float* dv,float *dw);